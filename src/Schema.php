<?php

namespace Aimanong\AmisPhp;

use Aimanong\AmisPhp\Component\Layout\{Page,Divider};
use Aimanong\AmisPhp\Component\Feedback\{Dialog,Drawer,Toast};

class Schema
{
    public function __call($name, $arguments)
    {
        $name = ucwords($name);
        return (new $name($arguments));
    }

    /**
     * 单页模式
     * @param string $title HTML页面标题
     * @param string|array $amisPageBody 主体数据
     * @param array $amisPropsData 顶部数据
     * @param array $pageParameter page组件的参数
     * @param string $backgroundImage
     * @return string
     */
    public function createSingle(string $title = '', string|array $amisPageBody = [], array $amisPropsData = [], array $pageParameter = [],string $backgroundImage = ''): string
    {
        $amisJson = (new Page(...$pageParameter))->appendBody($amisPageBody)->create();
        return (new Html(title: $title,  amisJson: $amisJson, amisPropsData: $amisPropsData,backgroundImage: $backgroundImage));
    }

    public function createMulti(string $title = '', string|array $amisPageBody = []): string
    {
//        $amisJson = [
//            'type' => 'app',
//            'brandName' => 'amis-php', //应用名称
//            'logo' => '', //logo图片
//            'header' => '', //头部
//            'footer' => '', //底部
//            'asideBefore' => '', //页面菜单上前面区域
//            'asideAfter' => '', //页面菜单下前面区域
//            'pages' => []  //菜单栏
//        ];
        return (new Html(title: $title,amisJson: $amisPageBody,model: 'multi'));
    }
}