<?php

namespace Aimanong\AmisPhp;

/**
 * 过滤器是对数据映射的一种增强，它的作用是对获取数据做一些处理
 */
class Filter
{
    /**
     * 输出HTML原文本
     * @param string $name 变量名
     * @param string $value html字符串
     * @return string
     */
    public static function html(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|html}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|html}';
        }
    }

    /**
     * 输出渲染后的HTML文本
     * @param string $name 变量名
     * @param string $value html字符串
     * @return string
     */
    public static function raw(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|raw}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|raw}';
        }
    }

    /**
     * 如果变量类型是JSON,输出JSON格式的字符串,如果是数组类型,输出数组格式的字符串,如果是其它的不处理直接输出
     * @param string $name
     * @return string
     */
    public static function json(string $name): string
    {
        return '${'.$name.'|json}';
    }

    /**
     * 字符串转整型数字
     * @param string $name
     * @param string $value
     * @return string
     */
    public static function toInt(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|toInt}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|toInt}';
        }
    }

    /**
     * 字符串转浮点数字
     * @param string $name
     * @param string $value
     * @return string
     */
    public static function toFloat(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|toFloat}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|toFloat}';
        }
    }

    /**
     * 返回当前时间并格式化输出
     * @param string $format 时间格式化格式
     * @return string
     */
    public static function now(string $format = 'YYYY-MM-DD HH\\:mm\\:ss'): string
    {
        if(empty($format) || $format === 'YYYY-MM-DD HH\\:mm\\:ss'){
            $format = 'YYYY-MM-DD HH\\:mm\\:ss';
        }else{
            $format = str_replace(['Y','m','d','H','i','s',':'],['YYYY','MM','DD','HH','mm','ss','\\:'],$format);
        }
        return '${_|now|date:'.$format.'}';
    }

    /**
     * 日期时间格式化过滤器,用于把特定时间值按指定格式输出
     * @param string $name 变量名
     * @param string $value 时间字符串
     * @param string $format 需要展示的格式,默认为LLL,如 2020年4月14日晚上7点59分
     * @param string $inputFormat 指定该变量值的格式,默认为x,即时间戳秒
     * @return string
     */
    public static function date(string $name = '', string $value = '', string $format = 'YYYY-MM-DD HH\\:mm\\:ss', string $inputFormat = 'x'): string
    {
        $str = '';
        if(!empty($name)){
            $str =  '${'.$name.'|date';
        }elseif(!empty($value)){
            $str = '${\''.$value.'\'|date';
        }
        if(empty($format)){
            $format = 'LLL';
        }elseif($format != 'YYYY-MM-DD HH\\:mm\\:ss'){
            $format = str_replace(['Y','m','d','H','i','s',':'],['YYYY','MM','DD','HH','mm','ss','\\:'],$format);
        }
        $str = $str.':'.$format;
        if(empty($inputFormat) || strtolower($inputFormat) === 'x'){
            $inputFormat = 'x';
        }else{
            $inputFormat = str_replace(['Y','m','d','H','i','s',':'],['YYYY','MM','DD','HH','mm','ss','\\:  '],$inputFormat);
        }
        return $str.':'.$inputFormat.'}';
    }

    /**
     * 日期修改，将输入的日期对象修改后返回新的日期对象
     * @param string $name 变量名
     * @param string $value 日期字符串
     * @param array $add 加 n（秒|分钟|小时|天|月|季度|年）
     * @param array $subtract 减 n（秒|分钟|小时|天|月|季度|年）
     * @param string $startOf  修改成某个维度的开头 例如: 年(本年度第一个月第一天0时0分0秒) 月(本月第一天0时0分0秒) 天(当天0时0分0秒)
     * @param string $endOf 修改成某个纬度的结尾 例如 年(本年度最后一个月最后一天23时59分59秒) 月(本月最后一天23时59分59秒) 天(当天23时59分59秒)
     * @return string
     */
    public static function dateModify(string $name = '', string $value = '', array|int $add = [], array|int $subtract = [],string $startOf = '', string $endOf = ''): string
    {
        $str = '';
        if(!empty($name)){
            $str =  '${'.$name;
        }elseif(!empty($value)){
            $str = '${\''.$value.'\'';
        }
        $str = self::addSubtract($str,'add',$add);
        $str = self::addSubtract($str,'subtract',$subtract);
        $str = self::startEndOf($str,'startOf',$startOf);
        $str = self::startEndOf($str,'endOf',$endOf);
        return $str.'}';
    }

    public static function addSubtract(string $str, string $name, array|string $data): string
    {
        if(!empty($data)){
            if(is_int($data)){
                $str = $str.'|dateModify:'.$name.':'.$data.':second';
            }elseif(is_array($data)){
                if(count($data) === 1){
                    if(is_int($data[0])){
                        $str = $str.'|dateModify:'.$name.':'.$data[0].':second';
                    }
                }else{
                    if(is_int($data[0]) && is_string($data[1])){
                        $str = $str.'|dateModify:'.$name.':'.$data[0].':'.self::dateType($data[1]);
                    }
                }
            }
        }
        return $str;
    }

    public static function startEndOf(string $str, string $name, string $data): string
    {
        return $str.'|dateModify:'.$name.':'.self::dateType($data);
    }

    public static function dateType(string $dateName): string
    {
       return match ($dateName){
            '分' => 'minute',
            '时' => 'hour',
            '天' => 'day',
            '月' => 'month',
            '季' => 'quarter',
            '年' => 'year',
            default => 'second' //默认为秒
       };
    }

    /**
     * 显示指定日期时间与现在的日期时间的相差时间,比如 1年前,2个月后,5秒钟前
     * @param string $name 变量名
     * @param string $value 日期时间字符串
     * @return string
     */
    public static function fromNow(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|fromNow}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|fromNow}';
        }
    }

    /**
     * 自动给数字加千分位逗号
     * @param string $name 变量名
     * @param string|int|float $value 数字值,可为字符串、整型、浮点型
     * @return string
     */
    public static function number(string $name = '', string|int|float $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|number}';
        }elseif(!empty($value)){
            return '${'.(is_string($value) ? '\''.$value.'\'' : $value).'|number}';
        }
    }

    /**
     * 把变量值前后的多余的空格去掉
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function trim(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|trim}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|trim}';
        }
    }

    /**
     * 小数转百分比,可以指定保留小数后几位
     * @param string $name 变量名
     * @param string|int|float $value 数字值
     * @param int $decimals 转成百分比后保留几位小数, 默认为0
     * @return string|void
     */
    public static function percent(string $name = '', string|int|float $value = '', int $decimals = 0)
    {
        if(!empty($name)){
            return '${'.$name.'|percent:'.$decimals.'}';
        }elseif(!empty($value)){
            return '${'.(is_string($value) ? '\''.$value.'\'' : $value).'|percent:'.$decimals.'}';
        }
    }

    /**
     * 把数值四舍五入后取值
     * @param string $name 变量名
     * @param string|int|float $value 数字值
     * @param int $decimals 保留几位小数,默认为2
     * @return string|void
     */
    public static function round(string $name = '', string|int|float $value = '', int $decimals = 2)
    {
        if(!empty($name)){
            return '${'.$name.'|round:'.$decimals.'}';
        }elseif(!empty($value)){
            return '${'.(is_string($value) ? '\''.$value.'\'' : $value).'|round:'.$decimals.'}';
        }
    }

    /**
     * 当超出若干字符时,后面的部分直接显示某字符
     * @param string $name 变量名
     * @param string $value 字符串值
     * @param int $length 指定多长的字符后省略,默认为 200
     * @param string $mask 省略时显示的字符, 默认为 "..."
     * @return string
     */
    public static function truncate(string $name = '', string $value = '', int $length = 200, string $mask = '...'): string
    {
        if(!empty($name)){
            return '${'.$name.'|truncate:'.$length.':\''.$mask.'\'}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|truncate:'.$length.':\''.$mask.'\'}';
        }
    }

    /**
     * 效果同 php 的 urlencode
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function urlEncode(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|url_encode}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|url_encode}';
        }
    }

    /**
     * 效果同 php 的 urldecode
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function urlDecode(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|url_decode}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|url_decode}';
        }
    }

    /**
     * 当变量值为空的时候,显示其它值代替
     * @param string $name 变量名
     * @param string $value 字符串
     * @param string $defaultValue 显示的替换默认值
     * @return string
     */
    public static function default(string $name = '', string $value = '', string $defaultValue = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|default:\''.$defaultValue.'\'}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|default:\''.$defaultValue.'\'}';
        }
    }

    /**
     * 将字符串通过分隔符分离成数组
     * @param string $name 变量名
     * @param string $value 字符串值
     * @param string $delimiter 分隔值,默认为 ,
     * @return string
     */
    public static function split(string $name = '', string $value = '', string $delimiter = ','): string
    {
        if(!empty($name)){
            return '${'.$name.'|split:\''.$delimiter.'\'}';
        }
    }

    /**
     * 获取数组中的第一个值
     * @param string $name 变量名
     * @return string
     */
    public static function firstArray(string $name): string
    {
        return '${'.$name.'|first}';
    }

    /**
     * 获取数组中的最后一个值
     * @param string $name
     * @return string
     */
    public static function lastArray(string $name): string
    {
        return '${'.$name.'|last}';
    }

    /**
     * 获取数组中的第 n 个值
     * @param string $name 变量名
     * @param int $nth 第几个值, 从 0 开始
     * @return string
     */
    public static function nthArray(string $name, int $nth = 0): string
    {
        return '${'.$name.'|nth:'.$nth.'}';
    }

    /**
     * 把秒值格式化成时间格式.比如 1分钟30秒  2年2季27天21时45分54秒
     * @param string $name 变量名
     * @param string|int $value 秒值
     * @return string
     */
    public static function duration(string $name = '', string|int $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|duration}';
        }elseif(!empty($value)){
            return '${'.(is_string($value) ? '\''.$value.'\'' : $value).'|duration}';
        }
    }

    /**
     * 将字符串转小写
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function lowerCase(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|lowerCase}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|lowerCase}';
        }
    }

    /**
     * 将字符串转大写
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function upperCase(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|upperCase}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|upperCase}';
        }
    }

    /**
     * 取字符串的一部分，第一个参数是起始，第二个参数的结束
     * @param string $name 变量名
     * @param string $value 字符串值
     * @param int $start 开始位置,从0开始
     * @param int $end 结束位置
     * @return string
     */
    public static function substring(string $name = '', string $value = '', int $start = 0, int $end = 100): string
    {
        if(!empty($name)){
            return '${'.$name.'|substring:'.$start.':'.$end.'}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|substring:'.$start.':'.$end.'}';
        }
    }

    /**
     * base64 加密
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function base64Encode(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|base64Encode}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|base64Encode}';
        }
    }

    /**
     * base64 解密
     * @param string $name 变量名
     * @param string $value 字符串值
     * @return string
     */
    public static function base64Decode(string $name = '', string $value = ''): string
    {
        if(!empty($name)){
            return '${'.$name.'|base64Decode}';
        }elseif(!empty($value)){
            return '${\''.$value.'\'|base64Decode}';
        }
    }

}