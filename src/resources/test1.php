<?php

use Aimanong\AmisPhp\Component\Ability\Action;
use Aimanong\AmisPhp\Component\Ability\App;
use Aimanong\AmisPhp\Component\Ability\ButtonGroup;
use Aimanong\AmisPhp\Component\Ability\DropDownButton;
use Aimanong\AmisPhp\Component\Layout\Grid;
use Aimanong\AmisPhp\Component\Layout\Page;
use Aimanong\AmisPhp\Schema;

require_once './vendor/autoload.php';

$schema = new Schema();
$app =  new App(brandName:'amis-php123',logo:'./logo.png',footer:'');
$grid = new Grid(align:'right',className:'border-b-2');
$grid->appendBody([
    'columnClassName' => '',
    'valign' => 'middle',
    'body' => [
        'type' => 'page',
        'className' => 'text-left bg-none',
        'body' => '左边'
    ]
]);

$userButton = new DropDownButton(label:'zyzyzzy',icon:'fas fa-user-cog',className:'mr-1.5',hideCaret:true,trigger:'hover',closeOnClick:true);
$userButton->appendBody((new Action(label:'个人信息',iconClassName:'mr-2',icon:'fa fa-info-circle'))->create());
$userButton->appendBody((new Action(label:'修改密码',iconClassName:'mr-2',icon:'fas fa-key'))->create());
$userButton->appendBody((new Action(label:'二次验证',iconClassName:'mr-2',icon:'fab fa-google'))->create());
$userButton->appendBody((new Action(label:'退出登录',iconClassName:'mr-2',icon:'fa fa-sign-out'))->create());

$grid->appendBody([
    'columnClassName' => 'text-right',
    'valign' => 'middle',
    'body' => [
        'type' => 'page',
        'className' => 'text-right bg-none',
        'body'=> [
            $userButton->create(),
            (new ButtonGroup())
                ->appendBody((new Action(label:'',block:false,icon:'fa fa-trash-o'))->create())
                ->appendBody((new Action(label:'',block:false,icon:'fas fa-envelope'))->create())
                ->appendBody((new Action(label:'',block:false,icon:'fas fa-bell'))->create())
                ->appendBody((new Action(label:'',block:false,icon:'fas fa-cogs'))->create())
                ->create(),

        ]
    ]
]);
$app->header = [
    $grid->create()
];
$app->footer = '<div class="p-2 text-center bg-light">底部区域</div>';
$app->asideBefore = [];
$app->asideAfter = [];
//$app->api = './site.json';
$app->pages[] = $app->appendPages(label: 'Home',url: '/',schema: (new Page(title: '欢迎页面',body: '欢迎页面'))->create());
$amisPageBody = $app->create();
exit($schema->createMulti('Page测试',$amisPageBody));