<?php
require_once './vendor/autoload.php';

use Aimanong\AmisPhp\Component\enum\DateShortcuts;
use Aimanong\AmisPhp\Component\enum\FormMode;
use Aimanong\AmisPhp\Component\FormData\Checkbox;
use Aimanong\AmisPhp\Component\FormData\Checkboxes;
use Aimanong\AmisPhp\Component\FormData\Combo;
use Aimanong\AmisPhp\Component\FormData\Form;
use Aimanong\AmisPhp\Component\FormData\InputCity;
use Aimanong\AmisPhp\Component\FormData\InputColor;
use Aimanong\AmisPhp\Component\FormData\InputDate;
use Aimanong\AmisPhp\Component\FormData\InputDateRange;
use Aimanong\AmisPhp\Component\FormData\InputDatetime;
use Aimanong\AmisPhp\Component\FormData\InputDatetimeRange;
use Aimanong\AmisPhp\Component\FormData\InputMonth;
use Aimanong\AmisPhp\Component\FormData\InputPassword;
use Aimanong\AmisPhp\Component\FormData\InputRichText;
use Aimanong\AmisPhp\Component\FormData\Switchs;
use Aimanong\AmisPhp\Component\Layout\Flex;
use Aimanong\AmisPhp\Schema;

$schema = new Schema();
$amisPageBody = [];
$flex = new Flex();
$form = new Form(title:'test表单',mode:FormMode::默认->value,debug:true);
$checkbox = new Checkbox(name:'testcheck',label:'勾选一下我',option:'选我选我');
$switch = (new Switchs(name:'testswitch',label:'勾选一下我',option:'选我选我'))->setOnText('你选择了我')->setOffText('没有选择我');
$checkboxes = (new Checkboxes(name:'testcheckboxes',label:'哈哈'))
    ->appendOptions('孙悟空','swk')
    ->appendOptions('猪八戒','zbj')
    ->appendOptions('沙和尚','shs')
    ->appendOptions('唐僧','ts');
$inputCity = new InputCity(name:'testcity',label:'选择一个城市');
$inputColor = new InputColor(name:'testinputcolor',label:'选择一种颜色');
$combo = (new Combo(label:'账号参数',multiple:true,value:[['color'=>'#ffffff']]))
    ->appendItems((new InputCity(label:'选择一个城市',name:'city'))->create())
    ->appendItems((new InputColor(label:'选择一种颜色',name:'color'))->create());
$inputDate = (new InputDate(label:'选择一个日期吧',value:date('Y-m-d'),minDate:date('Y-m-d')))
    ->appendShortcuts(DateShortcuts::今天)
    ->appendShortcuts(DateShortcuts::明天)
    ->appendShortcuts(DateShortcuts::昨天)
    ->appendShortcuts(DateShortcuts::本周一)
    ->appendShortcuts(DateShortcuts::本月初)
    ->appendShortcuts(DateShortcuts::本季度初);
$inputDatetime = (new InputDatetime(label:'选择一个年月日时分秒'))
    ->appendShortcuts(DateShortcuts::今天)
    ->appendShortcuts(DateShortcuts::明天)
    ->appendShortcuts(DateShortcuts::昨天)
    ->appendShortcuts(DateShortcuts::本周一)
    ->appendShortcuts(DateShortcuts::本月初)
    ->appendShortcuts(DateShortcuts::本季度初)
    ->setTimeConstraints();
$inputMonth = (new InputMonth(label:'选个月份'));
$inputDteRange = (new InputDateRange(label:'选择一个时间段'));
$inputDatetimeRange = (new InputDatetimeRange(label:'选择一个年月日时分秒段吧'));
//$codeEditor = (new CodeEditor(label:'代码编辑器',language:'php'));
$inputPassword = new InputPassword(label:'登录密码',name:'loginpassword');
$inputRichText = new InputRichText(label:'富文本',name:'testcontent');
$form->appendBody($checkbox->create());
$form->appendBody($switch->create());
$form->appendBody($checkboxes->create());
$form->appendBody($inputCity->create());
$form->appendBody($inputColor->create());
$form->appendBody($combo->create());
$form->appendBody($inputDate->create());
$form->appendBody($inputDatetime->create());
$form->appendBody($inputMonth->create());
$form->appendBody($inputDteRange->create());
$form->appendBody($inputDatetimeRange->create());
//$form->appendBody($codeEditor->create());
$form->appendBody($inputPassword->create());
$form->appendBody($inputRichText->create());
$item = [
    'style' => [
        'width' => '50%'
    ],
];
$itemForm = [...$item,...$form->create()];
//$flex->appendBody($item);
$flex->appendBody($itemForm);
//$flex->appendBody($item);
$amisPageBody[] = $flex->create();
exit($schema->createSingle('Page测试',$amisPageBody,'./src/resources'));