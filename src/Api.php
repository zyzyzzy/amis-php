<?php

namespace Aimanong\AmisPhp;

class Api
{
    public static function getUrl(string $url = ''): array
    {
        return self::url(url:$url);
    }

    public static function getJsonUrl(string $url = ''): array
    {
        return self::url(url:$url);
    }

    /**
     * application/x-www-form-urlencoded
     * @param string $url
     * @return string[]
     */
    public static function getFormUrl(string $url = ''): array
    {
        return self::url(url: $url, dataType: 'form');
    }

    /**
     * multipart/form-data 当表单项中文件类型数据，则自动使用multipart/form-data数据体
     * @param string $url
     * @return string[]
     */
    public static function getFormDataUrl(string $url = ''): array
    {
        return self::url(url: $url, dataType: 'form-data');
    }

    public static function getHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(url: $url, headers: $headers);
    }

    public static function getJsonHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(url: $url, headers: $headers);
    }

    public static function getFormHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(url: $url, dataType: 'form', headers: $headers);
    }

    public static function getFormDataHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(url: $url, dataType: 'form-data', headers: $headers);
    }

    public static function postUrl(string $url = ''): array
    {
        return self::url(method: 'post', url: $url);
    }

    public static function postJsonUrl(string $url = ''): array
    {
        return self::url(method: 'post', url: $url);
    }

    public static function postFormUrl(string $url = ''): array
    {
        return self::url(method: 'post', url: $url, dataType: 'form');
    }

    public static function postFormDataUrl(string $url = ''): array
    {
        return self::url(method: 'post', url: $url, dataType: 'form-data');
    }

    public static function postHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(method: 'post', url: $url, headers: $headers);
    }

    public static function postJsonHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(method: 'post', url: $url, headers: $headers);
    }

    public static function postFormHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(method: 'post', url: $url, dataType: 'form', headers: $headers);
    }

    public static function postFormDataHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(method: 'post', url: $url, dataType: 'form-data', headers: $headers);
    }

    public static function deleteUrl(string $url = ''): array
    {
        return self::url(method: 'delete', url: $url);
    }

    public static function deleteJsonUrl(string $url = ''): array
    {
        return self::url(method: 'delete', url: $url);
    }

    public static function deleteFormUrl(string $url = ''): array
    {
        return self::url(method: 'delete', url: $url, dataType: 'form');
    }

    public static function deleteFormDataUrl(string $url = ''): array
    {
        return self::url(method: 'delete', url: $url, dataType: 'form-data');
    }

    public static function deleteHeadersUrl(string $url = ''): array
    {
        return self::url(method: 'delete',url:$url);
    }
    public static function deleteJsonHeadersUrl(string $url = ''): array
    {
        return self::url(method: 'delete',url:$url);
    }
    public static function deleteFormHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(method: 'delete',url:$url,dataType: 'form', headers: $headers);
    }
    public static function deleteFormDataHeadersUrl(string $url = '', array $headers = []): array
    {
        return self::url(method: 'delete',url:$url,dataType: 'form-data', headers: $headers);
    }

    public static function setSendOn(array $data = [], string $sendOn = ''): array
    {
        return [...$data,...['sendOn'=>$sendOn]];
    }

    public static function setCache(array $data = [], int $cache = 0): array
    {
        return [...$data,...['cache'=>$cache]];
    }

    /**
     * @param string $method get,post,delete
     * @param string $url
     * @param string $dataType 请求格式 json(application/json), form(application/x-www-form-urlencoded), form-data(multipart/form-data)
     * @param array $headers //自定义请求头
     * @param string $sendOn 触发条件
     * @param int $cache 同样的请求缓存时间, 毫秒
     * @return array
     */
    public static function url(string $method = 'get', string $url = '', string $dataType = 'json', array $headers = [], string $sendOn = '', int $cache = 0): array
    {
        $data = [];
        empty($method) || $method == 'get' || $data['method'] = $method;
        $data['url'] = urldecode($url);
        empty($dataType) || $dataType == 'json' || $data['dataType'] = $dataType;
        empty($headers) || $data['headers'] = $headers;
        empty($sendOn) || $data['sendOn'] = $sendOn;
        empty($cache) || $data['cache'] = $cache;
        return $data;
    }
}