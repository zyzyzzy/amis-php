<?php

namespace Aimanong\AmisPhp\Component\enum;

enum ActionType: string
{
    case 默认 = 'button';
    case 异步请求 = 'ajax';
    case 单页跳转 = 'link';
    case 直接跳转 = 'url';
    case 弹框 = 'dialog';
    case 抽屉 = 'drawer';
    case 清空 = 'clear';
    case 复制 = 'copy';
    case 下载 = 'download';
    case 保存 = 'saveAs';
    case 刷新 = 'reload';
    case 重置 = 'reset';
    case 提交 = 'submit';
}
