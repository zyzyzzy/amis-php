<?php

namespace Aimanong\AmisPhp\Component\enum;

enum TabsMode: string
{
    case 线型 = 'line';
    case 卡片 = 'card';
    case 选择器型 = 'radio';
    case 垂直 = 'vertical';
    case 仿谷歌浏览器 = 'chrome';
    case 简约 = 'simple';
    case 加强 = 'strong';
    case 水平铺满 = 'tiled';
    case 侧边栏模式 = 'sidebar';
}
