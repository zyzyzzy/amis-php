<?php

namespace Aimanong\AmisPhp\Component\enum;

enum Level: string
{
    case 默认 = '';
    case 主要_蓝色 = 'primary';
    case 次要_灰色 = 'secondary';
    case 信息_蓝色 = 'info';
    case 成功_绿色 = 'success';
    case 警告_橙色 = 'warning';
    case 危险_红色 = 'danger';
    case 浅色_淡灰 = 'light';
    case 深色_黑色 = 'dark';
    case 链接_无边框背景色 = 'link';
}
