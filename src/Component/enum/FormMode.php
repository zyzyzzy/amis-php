<?php

namespace Aimanong\AmisPhp\Component\enum;

enum FormMode: string
{
    case 默认 = 'normal';
    case 水平 = 'horizontal';
    case 内联 = 'inline';
 }
