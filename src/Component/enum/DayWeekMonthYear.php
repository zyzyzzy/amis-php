<?php

namespace Aimanong\AmisPhp\Component\enum;

enum DayWeekMonthYear: string
{
    case 天 = 'days';
    case 周 = 'weeks';
    case 月 = 'months';
    case 年 = 'years';
}
