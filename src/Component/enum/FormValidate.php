<?php

namespace Aimanong\AmisPhp\Component\enum;

enum FormValidate: string
{
    case 必须是Email = 'isEmail,true';
    /**
     * isUrl 还可以以数组的形式配置三个参数
     * [
     *      'schemes' => ['http','https','ftp','sftp'] //协议
     *      'allowLocal' => true,  //是否允许填写本地地址
     *      'allowDataUrl' => true  //是否允许网址传参
     * ]
     */
    case 必须是Url = 'isUrl,true';
    case 必须是数值 = 'isNumeric,true';
    case 必须是字母 = 'isAlpha,true';
    case 必须是字母或者数值 = 'isAlphanumeric,true';
    case 必须是整数 = 'isInt,true';
    case 必须是浮点数 = 'isFloat,true';
    case 等于指定长度 = 'isLength';
    case 最小长度 = 'minLength';
    case 最大长度 = 'maxLength';
    case 最大数值 = 'maximum';
    case 最小数值 = 'minimum';
    case 等于哪个值 = 'equals';
    case 等于哪个变量 = 'equalsField';
    case 必须是JSON字符串 = 'isJson,true';
    case 必须是url路径 = 'isUrlPath,true';
    case 必须是手机号 = 'isPhoneNumber,true';
    case 必须是电话号码 = 'isTelNumber,true';
    case 必须为邮政编码 = 'isZipcode,true';
    case 必须为身份证号 = 'isId,true';
}
