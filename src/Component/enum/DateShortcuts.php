<?php

namespace Aimanong\AmisPhp\Component\enum;

enum DateShortcuts: string
{
    case 昨天 = 'yesterday';
    case 现在 = 'now';
    case 今天 = 'today';
    case 明天 = 'tomorrow';
    case 本周一 = 'thisweek';
    case 本周日 = 'endofthisweek';
    case 本月初 = 'thismonth';
    case 本月底 = 'endofthismonth';
    case 上月初 = 'prevmonth';
    case 上月底 = 'endoflastmonth';
    case 上季度初 = 'prevquarter';
    case 本季度初 = 'thisquarter';
    case n天前 = 'daysago';
    case n天后 = 'dayslater';
    case n周前 = 'weeksago';
    case n周后 = 'weekslater';
    case n月前 = 'monthsago';
    case n月后 = 'monthslater';
    case n季前 = 'quartersago';
    case n季后 = 'quarterslater';
    case n小时前 = 'hoursago';
    case n小时后 = 'hourslater';
}
