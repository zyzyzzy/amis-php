<?php

namespace Aimanong\AmisPhp\Component\enum;

enum Size: string
{
    case 极小 = 'xs';
    case 小 = 'sm';
    case 中等 = 'md';
    case 大 = 'lg';
    case 占满 = 'full';
}
