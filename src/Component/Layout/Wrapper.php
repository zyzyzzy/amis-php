<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class Wrapper extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Wrapper;
}