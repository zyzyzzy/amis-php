<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class Divider extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Divider;
}