<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class Tab extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Tab;
}