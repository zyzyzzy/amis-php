<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class CollapseGroup extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\CollapseGroup;
}