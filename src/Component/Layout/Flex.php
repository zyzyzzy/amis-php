<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class Flex extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Flex;
}