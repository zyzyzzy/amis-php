<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class Page extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Page;
}