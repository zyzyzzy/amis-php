<?php

namespace Aimanong\AmisPhp\Component\Layout;

use Aimanong\AmisPhp\Component\BaseClass;

class Tabs extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Tabs;

    /**
     * 添加tabs中的工具栏
     * @param array $toolbar
     * @return $this
     */
    public function appendToolbar(array $toolbar = []): static
    {
        $this->toolbar[] = $toolbar;
        return $this;
    }
}