<?php

namespace Aimanong\AmisPhp\Component;

use Aimanong\AmisPhp\Component\Ability\Action;

class Helper
{
    /**
     * 图标加标题
     * @param string $icon 图标
     * @param string $title 标题
     * @return array[]
     */
    public static function iconAndTitle(string $icon = '', string $title = ''): array
    {
        return [
            [
                'type' =>'icon',
                'icon' => $icon
            ],
            [
                'type' => 'tpl',
                'tpl' => $title,
                'className' => 'ml-1.5'
            ]
        ];
    }

    /**
     * 常用表单底部按钮
     * @param string $取消
     * @param string $重置
     * @param string $清空
     * @param string $提交
     * @return array
     */
    public static function usualFormActions(string $取消 = '', string $重置 = '', string $清空 = '', string $提交 = ''): array
    {
        $data = [];
        empty($取消) || $data[] = (new Action(label:'取消'))->取消关闭按钮();
        empty($重置) || $data[] = (new Action(label:'重置'))->重置表单按钮();
        empty($清空) || $data[] = (new Action(label:'清空'))->清空表单按钮();
        empty($提交) || $data[] = (new Action(label:'提交'))->提交表单按钮();
        return $data;
    }
}