<?php

namespace Aimanong\AmisPhp\Component;

use Aimanong\AmisPhp\Component\Feedback\Dialog;
use Aimanong\AmisPhp\Component\Feedback\Drawer;

class EventAction
{
    /**
     * 发送 http 请求
     * @param array $api 接口地址
     * @param string $messageSuccess 请求成功后的提示信息
     * @param string $messageFailed 请求失败后的提示信息
     * @param array $data 当前域的数据
     * @param bool $silent 是否静默模式,默认 否
     * @return array
     * 响应数据结构:
     * {
     *      //状态码
     *      "responseStatus": 0,
     *      //响应数据
     *      "responseData": {
     *          "xxx": "ssss"
     *      },
     *      // 响应信息
     *      "responseMsg": "ok"
     * }
     */
    public static function ajax(array $api = [], string $messageSuccess = '', string $messageFailed = '', array $data = [], bool $silent = false): array
    {
        $action = [];
        $action['actionType'] = 'ajax';
        $action['args']['api'] = $api;
        if(!empty($messageSuccess)){
            $action['args']['message']['success'] = $messageSuccess;
        }
        if(!empty($messageFailed)){
            $action['args']['message']['failed'] = $messageFailed;
        }
        if(!empty($silent)){
            $action['args']['options']['silent'] = true;
        }
        if(!empty($data)){
            $action['data'] = $data;
        }
        return $action;
    }

    /**
     * 打开关闭弹窗(模态)
     * @param Dialog|array $dialog
     * @param string $componentId
     * @param string $status
     * @return array
     */
    public static function dialog(Dialog|array $dialog = [], string $componentId = '', string $status = 'open'): array
    {
        $action  = [];
        if($status === 'open'){
            $action['actionType'] = 'dialog';
            $action['args']['dialog'] = is_array($dialog) ? $dialog: $dialog->create();
        }elseif($status === 'close'){
            $action['actionType'] = 'closeDialog';
            if(!empty($componentId)){
                $action['componentId'] = $componentId;
            }elseif($dialog instanceof Dialog){
                $action['componentId'] = $dialog->id;
            }
        }
        return $action;
    }

    /**
     * 打开关闭抽屉(模态)
     * @param Drawer|array $drawer
     * @param string $componentId
     * @param string $status
     * @return array
     */
    public static function drawer(Drawer|array $drawer = [], string $componentId = '', string $status = 'open'): array
    {
        $action  = [];
        if($status === 'open'){
            $action['actionType'] = 'drawer';
            $action['args']['drawer'] = is_array($drawer) ? $drawer: $drawer->create();
        }elseif($status === 'close'){
            $action['actionType'] = 'closeDrawer';
            if(!empty($componentId)){
                $action['componentId'] = $componentId;
            }elseif($drawer instanceof Drawer){
                $action['componentId'] = $drawer->id;
            }
        }
        return $action;
    }

    /**
     * 打开对话框
     * @param string $title
     * @param string $msg
     * @return array
     */
    public static function alert(string $title = '', string $msg = ''): array
    {
        $action = [];
        $action['actionType'] = 'alert';
        $action['args']['title'] = $title;
        $action['args']['msg'] = $msg;
        return $action;
    }

    /**
     * 跳转链接(可以用${xxx}取值)
     * @param string $url 跳转地址
     * @param bool $blank 是否在新页面打开, 默认 否
     * @param array $params 页面参数 会转换为地址参数并用&连接
     * @return array
     */
    public static function jumpUrl(string $url = '', bool $blank = true, array $params = []): array
    {
        $action = [];
        $action['actionType'] = 'url';
        $action['args']['url'] = $url;
        $action['args']['blank'] = $blank;
        $action['args']['params'] = $params;
        return $action;
    }

    /**
     * 跳转单页(amis平台内的页面,可以用${xxx}取值)
     * @param string $link
     * @param array $params
     * @return array
     */
    public static function jumpLink(string $link = '', array $params = []): array
    {
        $action = [];
        $action['actionType'] = 'link';
        $action['args']['link'] = $link;
        $action['args']['params'] = $params;
        return $action;
    }

    /**
     * 浏览器前进/后退到指定页面
     * @param int $delta 正数为前进,负数为后退
     * @return array
     */
    public static function goPage(int $delta = 0): array
    {
        $action = [];
        $action['actionType'] = 'goPage';
        $action['args']['delta'] = $delta;
        return $action;
    }

    /**
     * 浏览器刷新
     * @return array
     */
    public static function refresh(): array
    {
        $action = [];
        $action['actionType'] = 'refresh';
        return $action;
    }

    /**
     * toast 提示
     * @param string $msg 消息内容
     * @param string $msgType 消息类型
     * @param string $position 提示显示位置
     * @param bool $closeButton 是否展示关闭按钮 默认 否
     * @param bool $showIcon 是否展示图标 默认 是
     * @param int $timeout 持续时间 默认5000毫秒,error类型默认6000毫秒
     * @return array
     */
    public static function toast(string $msg = '', string $msgType = 'info', string $position = 'top-center', bool $closeButton = false, bool $showIcon = true, int $timeout = 5000): array
    {
        $action = [];
        $action['actionType'] = 'toast';
        $action['args']['msg'] = $msg;
        if(empty($msgType) || $msgType === 'info'){
            $action['args']['msgType'] = $msgType;
        }else{
            $action['args']['msgType'] = match ($msgType){
                'success' => 'success',
                'error' => 'error',
                'warning'  => 'warning',
                default => 'info'
            };
        }
        if(empty($position) || $position === 'top-center'){
            $action['args']['position'] = 'top-center';
        }else {
            $action['args']['position'] = match ($position) {
                'top-right' => 'top-right',
                'top-left' => 'top-left',
                'bottom-center' => 'bottom-center',
                'bottom-left' => 'bottom-left',
                'bottom-right' => 'bottom-right',
                default => 'top-center'
            };
        }
        if($closeButton === true){
            $action['args']['closeButton'] = true;
        }
        if($showIcon === false){
            $action['args']['showIcon'] = false;
        }
        if(empty($timeout)){
            if($msgType === 'error'){
                $action['args']['timeout'] = 6000;
            }else{
                $action['args']['timeout'] = 5000;
            }
        }else{
            $action['args']['timeout'] = $timeout;
        }
        return $action;
    }

    /**
     * 复制
     * @param string $content 复制内容
     * @param string $copyFormat 复制模式,普通字符串或富文本,默认普通字符串
     * @return array
     */
    public static function copy(string $content, string $copyFormat = ''): array
    {
        $action = [];
        $action['actionType'] = 'copy';
        $action['args']['content'] = $content;
        if(!empty($copyFormat) && $copyFormat === 'html'){
            $action['args']['copyFormat'] = 'text/html';
        }
        return $action;
    }

    /**
     * 刷新amis组件
     * 仅支持form、wizard、service、page、app、chart、crud
     * @param string $componentId
     * @param bool $resetPage 当目标组件为 crud 时，可以控制是否重置页码
     * @return array
     */
    public static function reload(string $componentId = '', bool $resetPage = true): array
    {
        $action = [];
        $action['actionType'] = 'reload';
        $action['args']['componentId'] = $componentId;
        if($resetPage === false){
            $action['resetPage'] = false;
        }
        return $action;
    }

    /**
     * 显示与隐藏amis组件
     * @param string $componentId 组件ID
     * @param string $showHidden 显示(show)还是隐藏(hidden), 默认 show
     * @return array
     */
    public static function showHidden(string $componentId = '', string $showHidden = 'show'): array
    {
        $action = [];
        if(empty($showHidden) || $showHidden === 'show'){
            $action['actionType'] = 'show';
        }else{
            $action['actionType'] = 'hidden';
        }
        $action['args']['componentId'] = $componentId;
        return $action;
    }

    /**
     * 控制amis组件状态, 启用还是禁用
     * @param string $componentId 组件ID
     * @param string $enabledDisabled 启用(enabled)还是禁用(disabled), 默认启用
     * @return array
     */
    public static function enabledDisabled(string $componentId = '', string $enabledDisabled = 'enabled'): array
    {
        $action = [];
        if(empty($enabledDisabled) || $enabledDisabled === 'enabled'){
            $action['actionType'] = 'enabled';
        }else{
            $action['actionType'] = 'disabled';
        }
        $action['args']['componentId'] = $componentId;
        return $action;
    }

    /**
     * 触发其他组件的动作
     * @param string $componentId 组件ID
     * @param array $args 组件动作参数
     * @return array
     */
    public static function changeActiveKey(string $componentId = '', array $args = []): array
    {
        $action = [];
        $action['actionType'] = 'changeActiveKey';
        $action['componentId'] = $componentId;
        if(!empty($args)){
            $action['args'] = $args;
        }
        return $action;
    }
}