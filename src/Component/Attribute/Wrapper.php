<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Wrapper
{
    const TYPE = 'wrapper';
    public string $className = '';
    public string $size = ''; //支持 xs, sm, md 和 lg
    public string|array $style = '';
    public array $body = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        if(!empty($this->className)){
            $data['className'] = $this->className;
        }
        if(!empty($this->size)){
            $data['size'] = $this->size;
        }
        if(!empty($this->style)){
            $data['style'] = $this->style;
        }
        $data['body'] = $this->body;
        return $data;
    }

    public function appendBody(array|string $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}