<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputCity
{
    const TYPE = 'input-city';
    public bool $allowCity = true; //允许选择城市
    public bool $allowDistrict = true; //允许选择区域
    public bool $searchable = true; //是否出搜索框
    public bool $extractValue = false; //默认 true 是否抽取值，如果设置成 false 值格式会变成对象，包含 code、province、city 和 district 文字信息。
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        $data['allowCity'] = $this->allowCity;
        $data['allowDistrict'] = $this->allowDistrict;
        $data['searchable'] = $this->searchable;
        $data['extractValue'] = $this->extractValue;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {

    }
}