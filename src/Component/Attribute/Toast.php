<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Toast
{
    public function create(): array
    {
        return [];
    }
    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}