<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Grid
{
    const TYPE = 'grid';
    public string $className = '';
    public string $gap = ''; //水平间距 'xs' | 'sm' | 'base' | 'none' | 'md' | 'lg'
    public string $valign = ''; //垂直对齐方式 'top' | 'middle' | 'bottom' | 'between'
    public string $align = ''; //水平对齐方式 'left' | 'right' | 'between' | 'center'
    public array $columns = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->className) || $data['className'] = $this->className;
        if(!empty($this->gap)){
            $data['gap'] = $this->gap;
        }
        if(!empty($this->valign)){
            $data['valign'] = $this->valign;
        }
        if(!empty($this->align)){
            $data['align'] = $this->align;
        }
        $data['columns'] = $this->columns;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->columns[] = $content;
        return $this;
    }
}