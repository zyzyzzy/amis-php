<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Dialog
{
    const ACTIONTYPE = 'dialog';
    public string|array $title = '';
    public array $body = [];
    public string $size = '';
    public string $bodyClassName = '';
    public bool $closeOnEsc = false;  //是否支持按 Esc 关闭 Dialog
    public bool $showCloseButton = true; //是否显示右上角的关闭按钮
    public bool $showErrorMsg = true; //是否在弹框左下角显示报错信息
    public bool $showLoading = true; //是否在弹框左下角显示 loading 动画
    public bool $disabled = false; //如果设置此属性，则该 Dialog 只读没有提交操作。
    public array|string $actions = ''; //默认【确认】和【取消】如果想不显示底部按钮，可以配置：[]
    public string|array $reload = ''; //新增或修改后刷新目标

    public function create(): array
    {
        $data = [];
        $data['actionType'] = self::ACTIONTYPE;
        $data['dialog']['title'] = $this->title;
        $data['dialog']['body'] = $this->body;
        $data['dialog']['size'] = $this->size;
        empty($this->bodyClassName) || $data['dialog']['bodyClassName'] = $this->bodyClassName;
        $this->closeOnEsc === false || $data['dialog']['closeOnEsc'] = true;
        $this->showCloseButton === true || $data['dialog']['showCloseButton'] = false;
        $this->showErrorMsg === true || $data['dialog']['showErrorMsg'] = false;
        $this->showLoading === true || $data['dialog']['showLoading'] = false;
        $this->disabled === false || $data['dialog']['disabled'] = true;
        $this->actions === '' || $data['dialog']['actions'] = $this->actions;
        if(!empty($this->reload)){
            if(is_string($this->reload)){
                $data['reload'] = $this->reload;
            }elseif(is_array($this->reload)){
                $data['reload'] = implode(',',$this->reload);
            }
        }
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}