<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Switchs
{
    const TYPE = 'switch';
    public string $option = ''; //选项说明
    public string|array $onText = ''; //开启时开关显示的内容
    public string|array $offText = ''; //关闭时开关显示的内容
 //   private string $size = 'md'; //开关大小 md sm

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        empty($this->option) || $data['option'] = $this->option;
        empty($this->onText) || $data['onText'] = $this->onText;
        empty($this->offText) || $data['offText'] = $this->offText;
        empty($this->size) || $this->size === 'md' || $data['size'] = 'sm';
        $data['trueValue'] = 1;
        $data['falseValue'] = 0;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {

    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}