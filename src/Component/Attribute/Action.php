<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Action
{
    const TYPE = 'action';
    public string $actionType = ''; //action 作用类型
    public string $label = '';  //按钮文本, 可以用 ${xxx}来取值
    public string $level = 'default'; //按钮样式
    public string $size = '';  //按钮大小
    public string $icon = '', $leftIcon = '';  //设置图标 左边
    public string $iconClassName = '', $leftIconClassName = '';  //给图标添加类名 左边
    public string $rightIcon = '';  //设置图标 右边
    public string $rightIconClassName = ''; //给图标添加类名 右边
    public bool $block = false; //以整块来显示按钮
    public string $confirmText = ''; // 当设置后，操作在开始前会询问用户。可用 ${xxx} 取值。
    public string|array $reload = ''; //指定此次操作完后，需要刷新的目标组件名字（组件的name值，自己配置的），多个请用 , 号隔开。
    public string|array $tooltip = ''; //鼠标停留时弹出该段文字，也可以配置对象类型：字段为title和content。可用 ${xxx} 取值。
    public string|array $disabledTip = ''; //被禁用后鼠标停留时弹出该段文字，也可以配置对象类型：字段为title和content。可用 ${xxx} 取值。
    public string $tooltipPlacement = 'top'; //如果配置了tooltip或者disabledTip，指定提示信息位置，可配置top、bottom、left、right。
    public array $body = [];
    public bool $loading = false;  //显示按钮 loading 效果
    public string $loadingOn = '';  //显示按钮 loading 表达式
    public string $className = '';

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        empty($this->actionType) || $data['actionType'] = $this->actionType;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->level) || $data['level'] = $this->level;
        empty($this->size) || $data['size'] = $this->size;
        empty($this->icon) || $data['icon'] = $this->icon;
        empty($this->leftIcon) || $data['icon'] = $this->leftIcon;
        empty($this->iconClassName) || $data['iconClassName'] = $this->iconClassName;
        empty($this->leftIconClassName) || $data['iconClassName'] = $this->leftIconClassName;
        empty($this->rightIcon) || $data['rightIcon'] = $this->rightIcon;
        empty($this->rightIconClassName) || $data['rightIconClassName'] = $this->rightIconClassName;
        empty($this->block) || $data['block'] = true;
        empty($this->confirmText) || $data['confirmText'] = $this->confirmText;
        empty($this->reload) || $data['reload'] = $this->reload;
        empty($this->tooltip) || $data['tooltip'] = $this->tooltip;
        empty($this->disabledTip) || $data['disabledTip'] = $this->disabledTip;
        empty($this->tooltipPlacement) || $this->tooltipPlacement === 'top' || $data['tooltipPlacement'] = $this->tooltipPlacement;
        $this->loading === false || $data['loading'] = true;
        empty($this->loadingOn) || $data['loadingOn'] = $this->loadingOn;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}