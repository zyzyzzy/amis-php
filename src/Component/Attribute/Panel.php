<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Panel
{
    const TYPE = 'panel';
    public string $className = '';
    public string|array $title = '';
    public array $header = [];
    public array $body = [];
    public array $footer = [];
    public bool $affixFooter = false;
    public array $actions = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        if(!empty($this->className)){
            $data['className'] = $this->className;
        }
        if(!empty($this->title)){
            $data['title'] = $this->title;
        }
        if(!empty($this->header)){
            $data['header'] = $this->header;
        }
        if(!empty($this->footer)){
            $data['footer'] = $this->footer;
        }
        if(!empty($this->body)){
            $data['body'] = $this->body;
        }
        if(!empty($this->actions)){
            $data['actions'] = $this->actions;
        }
        if(!empty($this->affixFooter)){
            $data['affixFooter'] = true;
        }
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}