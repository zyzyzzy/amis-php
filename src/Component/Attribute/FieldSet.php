<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait FieldSet
{
    const TYPE = 'fieldSet';
    public string $className = '';
    public string $headingClassName = '';
    public string $bodyClassName = '';
    public string $title = '';
    public string|array $body = [];
    public string $mode = 'normal'; //表单展示方式，可以是：normal(默认)、horizontal(水平) 或者 inline(内联)
    public bool $collapsable = false;
    public bool $collapsed = false;
    public string|array $collapseTitle = '';
    public string $size = ''; //大小，支持 xs、sm、base、md、lg
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->headingClassName) || $data['headingClassName'] = $this->headingClassName;
        empty($this->bodyClassName) || $data['bodyClassName'] = $this->bodyClassName;
        $data['title'] = $this->title;
        $data['body'] = $this->body;
        $data['mode'] = $this->mode;
        $this->collapsable === false || $data['collapsable'] = true;
        $this->collapsed === false || $data['collapsed'] = true;
        empty($this->collapseTitle) || $data['collapseTitle'] = $this->collapseTitle;
        empty($this->size) || $data['size'] = $this->size;
        return $data;

    }
    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}