<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait CollapseGroup
{
    const TYPE = 'collapse-group';
    public string|int $activeKey = '';  //初始化激活面板的key
    public bool $accordion = false;  //是否手风琴模式（一次只能打开一个）
    public string $expandIconPosition = 'left'; //设置图标位置 可选值 left|right
    public array $body = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        if(!empty($this->activeKey)){
            $data['activeKey'] = $this->activeKey;
        }
        if($this->accordion === true){
            $data['accordion'] = true;
        }
        if(!empty($this->expandIconPosition) && $this->expandIconPosition === 'right'){
            $data['expandIconPosition'] = 'right';
        }
        $data['body'] = $this->body;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}