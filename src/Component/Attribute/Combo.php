<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Combo
{
    const TYPE = 'combo';
    public string $formClassName = ''; //单组表单的类名
    public bool $noBorder = false; //单组表单是否显示边框
    public array $scaffold = [];  //单组表单项初始值
    public bool $multiple = false; //多选模式下先自动显示新增按钮
    public bool $multiLine = false; //false 横着展示一排, true 竖着展示
    public int $minLength = 0; //最少添加的条数
    public int $maxLength = 0; //最多添加的条数
    public bool $addable = true; //是否显示新增按钮,默认显示
    public bool $removable = true; //是否显示删除按钮
    public string|array $deleteApi = ''; //如果配置了，则删除前会发送一个 api，请求成功才完成删除
    public string $deleteConfirmText = ''; //当配置 deleteApi 才生效！删除时用来做用户确认
    public bool $draggable = false; //是否可以拖动排序, 需要注意的是当启用拖动排序的时候，会多一个$id 字段
    public string $draggableTip = ''; //可拖拽的提示文字 新增按钮右边
    public string $placeholder = ''; //没有成员时显示的内容
    public bool $canAccessSuperData = false; //指定是否可以自动获取上层的数据并映射到表单项上
    public bool $strictMode = true; //如果为 false 时会同步更新内部同名表单项
    public array $syncFields = []; //需要同步的变量名
    public array $items = []; //组合展示的表单项
    public array $conditions = []; //新增分类
    public string $itemClassName = ''; //单组 CSS 类
    public string $itemsWrapperClassName = ''; //组合区域 CSS 类
    public array $addBtn = []; //可新增自定义配置渲染新增按钮，在tabsMode: true下不生效。
    public string $addButtonClassName = ''; //新增按钮 CSS 类名
    public string $addButtonText = '新增'; //新增按钮文字
    public string|array $deleteBtn = ''; //只有当removable为 true 时有效; 如果为string则为按钮的文本；如果为Button则根据配置渲染删除按钮。
    public bool $tabsMode = false; //是否启用Tabs模式
    public string $tabsStyle = 'line'; //样式 line card radio
    public string $tabsLabelTpl = '${index|plus}'; //用来生成标题的模板

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        if($this->multiple === false){
            $this->noBorder === false || $data['noBorder'] = true;
            empty($this->scaffold) || $data['scaffold'] = $this->scaffold;
        }else{
            $data['multiple'] = true;
            empty($this->minLength) || $data['minLength'] = $this->minLength;
            empty($this->maxLength) || $data['maxLength'] = $this->maxLength;
            $this->addable === true || $data['addable'] = false;
            $this->removable === true || $data['removable'] = false;
            if(!empty($this->deleteApi)){
                $data['deleteApi'] = $this->deleteApi;
                empty($this->deleteConfirmText) || $data['deleteConfirmText'] = $this->deleteConfirmText;
            }
            $this->draggable === false || $data['draggable'] = true;
            empty($this->draggableTip) || $data['draggableTip'] = $this->draggableTip;
            empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
            empty($this->addBtn) || $data['addBtn'] = $this->addBtn;
            empty($this->addButtonText) || $this->addButtonText === '新增' || $data['addButtonText'] = $this->addButtonText;
            empty($this->addButtonClassName) || $data['addButtonClassName'] = $this->addButtonClassName;
            empty($this->deleteBtn) || $data['deleteBtn'] = $this->deleteBtn;
            $this->tabsMode === false || !($data['tabsMode'] = true) || empty($this->tabsStyle) || $this->tabsStyle === 'line' || $data['tabsStyle'] = $this->tabsStyle;

            empty($this->tabsLabelTpl) || $this->tabsLabelTpl === '${index|plus}' || $data['tabsLabelTpl'] = $this->tabsLabelTpl;
            empty($this->items) || $data['items'] = $this->items;
            empty($this->conditions) || $data['conditions'] = $this->conditions;
        }
        empty($this->value) || $data['value'] = $this->value;
        if($this->canAccessSuperData === true){
            $data['canAccessSuperData'] = true;
            $this->strictMode === true || $data['strictMode'] = $this->strictMode;
            empty($this->syncFields) || $data['syncFields'] = $this->syncFields;
        }

        $this->multiLine === false || $data['multiLine'] = true;
        empty($this->itemClassName) || $data['itemClassName'] = $this->itemClassName;
        empty($this->itemsWrapperClassName) || $data['itemsWrapperClassName'] = $this->itemsWrapperClassName;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        
    }
}