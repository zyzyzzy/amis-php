<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait TooltipWrapper
{
    const TYPE = 'tooltip-wrapper';
    public string $title = ''; //文字提示标题
    public string $content = '';  //文字提示内容, 兼容之前的 tooltip 属性
    public string $placement = 'top'; //文字提示浮层出现位置
    public string $tooltipTheme = 'light'; //主题样式，light|dark 默认为 light
    public array $offset = [0,0]; //文字提示浮层位置相对偏移量，单位 px 上和左
    public bool $showArrow = true; //是否展示浮层指向箭头
    public bool $enterable = true; //是否鼠标可以移入到浮层中
    public bool $disabled = false; //是否禁用浮层提示
    public array $trigger = ['hover']; //浮层触发方式，支持数组写法["hover", "click","focus"]
    public int $mouseEnterDelay = 0; //浮层延迟展示时间，单位 ms
    public int $mouseLeaveDelay = 300; //浮层延迟隐藏时间，单位 ms
    public bool $rootClose = true; //是否点击非内容区域关闭提示
    public bool $inline = false; //内容区是否内联显示
    public string|array $body = []; //内容容器
    public string $className = ''; //内容区类名
    public string $tooltipClassName = ''; //文字提示浮层类名

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        empty($this->title) || $data['title'] = $this->title;
        $data['content'] = $this->content;
        empty($this->placement) || $this->placement === 'top' || $data['placement'] = $this->placement;
        empty($this->tooltipTheme) || $this->tooltipTheme === 'light' || $data['tooltipTheme'] = 'dark';
        empty($this->offset) || ($this->offset[0] === 0 && $this->offset[1] === 0) || $data['offset'] = $this->offset;
        $this->showArrow === true || $data['showArrow'] = false;
        $this->enterable === true || $data['enterable'] = false;
        $this->disabled === false || $data['disabled'] = true;
        empty($this->mouseEnterDelay) || $data['mouseEnterDelay'] = $this->mouseEnterDelay;
        empty($this->mouseLeaveDelay) || $this->mouseLeaveDelay === 300 || $data['mouseLeaveDelay'] = $this->mouseLeaveDelay;
        $this->rootClose === true || $data['rootClose'] = false;
        $this->inline === false || $data['inline'] = true;
        $data['body'] = $this->body;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->tooltipClassName) || $data['tooltipClassName'] = $this->tooltipClassName;
        return $data;

    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}