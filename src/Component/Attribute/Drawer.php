<?php

namespace Aimanong\AmisPhp\Component\Attribute;


trait Drawer
{
    const ACTIONTYPE = 'drawer';
    public string $title = '';
    public array $body = [];
    public string $size = '';
    public string $position = 'left'; //弹出的方向 left,right,top,bottom
    public string $className = '';
    public string $headerClassName = '';
    public string $bodyClassName = '';
    public string $footerClassName = '';
    public bool $showCloseButton = true; //是否显示关闭按钮
    public bool $closeOnEsc = false; //是否按ESC关闭
    public bool $closeOnOutside = false; //点内容区外是否关闭
    public bool $overlay = true; // 是否显示蒙层
    public bool $resizable = false; // 是否可以拖拽
    public string|int $width = 500; //容器宽度, 方向为 left 或 right 时有效
    public string|int $height = 500; //容器高度, 方向为 top 或 bottom 时有效
    public string|array $actions = ''; //默认【确认】和【取消】如果想不显示底部按钮，可以配置：[]
    public function create(): array
    {
        $data = [];
        $data['actionType'] = self::ACTIONTYPE;
        $data['drawer']['title'] = $this->title;
        $data['drawer']['body'] = $this->body;
        empty($this->size) || $data['drawer']['size'] = $this->size;
        empty($this->position) || $this->position === 'right' || $data['drawer']['position'] = $this->position;
        empty($this->headerClassName) || $data['drawer']['headerClassName'] = $this->headerClassName;
        empty($this->bodyClassName) || $data['drawer']['bodyClassName'] = $this->bodyClassName;
        empty($this->footerClassName) || $data['drawer']['footerClassName'] = $this->footerClassName;
        $this->showCloseButton === true || $data['drawer']['showCloseButton'] = false;
        $this->closeOnEsc === false || $data['drawer']['closeOnEsc'] = true;
        $this->closeOnOutside === false || $data['drawer']['closeOnOutside'] = true;
        $this->overlay === true || $data['drawer']['overlay'] = false;
        $this->resizable === false || $data['drawer']['resizable'] = true;
        empty($this->width) || $this->width === 500 || $data['drawer']['width'] = $this->width;
        empty($this->height) || $this->height === 500 || $data['drawer']['height'] = $this->height;
        $this->actions === '' || $data['drawer']['actions'] = $this->actions;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}