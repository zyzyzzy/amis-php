<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Tpl
{
    const TYPE = 'tpl';
    public string $className = '';
    public string $tpl = '';
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        if(!empty($this->className)){
            $data['className'] = $this->className;
        }
        $data['tpl'] = $this->tpl;
        if(!empty($this->onEvent)){
            $data['onEvent'] = $this->onEvent;
        }
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->tpl .= $content;
        return $this;
    }
}