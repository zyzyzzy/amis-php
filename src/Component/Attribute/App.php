<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait App
{
    const TYPE = 'app';
    public string $brandName = 'amis-php'; //应用名称
    public string $logo = ''; //LOGO图标,支付图标地址或svg
    public string $className = '';  //css类名
    public string|array $header = []; //顶部区域
    public string|array $footer = []; //底部区域
    public string|array $asideBefore = []; // 页面菜单上前面区域
    public string|array $asideAfter = []; //页面菜单下前面区域
    public string $api = ''; //页面配置接口,如果你想远程拉取页面配置请配置
    public array $pages = []; //具体的页面配置。 通常为数组，数组第一层为分组，一般只需要配置 label 集合，如果你不想分组，直接不配置，真正的页面请在第二层开始配置，即第一层的 children 中。
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        $data['id'] = $this->id;
        $data['brandName'] = $this->brandName;
        $data['logo'] = $this->logo;
        $data['className'] = $this->className;
        $data['header'] = $this->header;
        $data['footer'] = $this->footer;
        $data['asideBefore'] = $this->asideBefore;
        $data['asideAfter'] = $this->asideAfter;
        if(!empty($this->api)){
            $data['api'] = $this->api;
        }
        if(!empty($this->pages)){
            $data['pages'] = $this->pages;
        }
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->pages[] = $content;
        return $this;
    }
}