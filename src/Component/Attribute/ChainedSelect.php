<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait ChainedSelect
{
    const TYPE = 'chained-select';
    public string $value = '';
    public string|array $source = '';
    public string $label = '';
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        $data['value'] = $this->value;
        $data['source'] = $this->source;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {

    }
}