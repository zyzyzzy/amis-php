<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Tab
{
    public string $title = ''; //Tab标题
    public string $icon = ''; //Tab 的图标
    public string $iconPosition = 'left'; //Tab 的图标位置 left|right
    public array $tab = [];
    public string $hash = ''; //设置以后将跟 url 的 hash 对应
    public bool $reload = false; //设置以后内容每次都会重新渲染，对于 crud 的重新拉取很有用
    public bool $unmountOnExit = false; //每次退出都会销毁当前 tab 栏内容
    public string $className = '';
    public bool $closable = false;
    public bool $disabled = false;

    public function create(): array
    {
        $data = [];
        $data['title'] = $this->title;
        if(!empty($this->icon)){
            $data['icon'] = $this->icon;
            $data['iconPosition'] = $this->iconPosition;
        }
        if(!empty($this->hash)){
            $data['hash'] = $this->hash;
        }
        if($this->reload === true){
            $data['reload'] = true;
        }
        if($this->unmountOnExit === true){
            $data['unmountOnExit'] = true;
        }
        if($this->closable === true){
            $data['closable'] = true;
        }
        if($this->disabled === true){
            $data['disabled'] = true;
        }
        if(!empty($this->className)){
            $data['className'] = $this->className;
        }
        $data['tab'] = $this->tab;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->tab[] = $content;
        return $this;
    }
}