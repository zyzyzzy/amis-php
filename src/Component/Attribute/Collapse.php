<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Collapse
{
    const TYPE = 'collapse';
    public bool $disabled = false; //是否禁用
    public bool $collapsed = true; //初始状态是否折叠
    public string|int $key = ''; //标识
    public bool $showArrow = true; //是否展示图标
    public string|array $header = ''; //标题
    public array $body = []; //内容

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        if($this->disabled === true){
            $data['disabled'] = true;
        }
        if($this->collapsed === false){
            $data['collapsed'] = false;
        }
        if($this->showArrow === false){
            $data['showArrow'] = false;
        }
        $data['key'] = $this->key;
        $data['header'] = $this->header;
        $data['body'] = $this->body;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}