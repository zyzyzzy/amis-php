<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Radios
{
    const TYPE = 'radios';
    public int $columnsCount = 1;
    public bool $selectFirst = false;
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->source) || $data['source'] = $this->source;
        $this->inline === true || $data['inline'] = false;
        $this->inline === true || empty($this->columnsCount) || $this->columnsCount === 1 || $data['columnsCount'] = $this->columnsCount;
        $this->selectFirst === false || $data['selectFirst'] = true;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        $this->options[] = $content;
        return $this;
    }
}