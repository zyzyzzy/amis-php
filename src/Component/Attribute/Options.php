<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Options
{
    use FormItem;
    public string|array $source = '';
    public bool $multiple = false ; //是否支持多选
    public bool $searchable = false; //前端检索
    public bool $creatable = false; //是否有新增功能
    public string $createBtnLabel = ''; //自定义新增按钮和弹框的标题
    public string $optionLabel = ''; //替换"选项"这个字符，如我们配置"optionLabel": "员工"，标题会显示：新增员工 ,新增和编辑都有效
    public array $addControls = []; //自定义新增表单项
    public array $addDialog = []; //配置新增弹框其它属性
    public string|array $addApi = ''; //配置新增接口
    public bool $editable = false; // 是否有编辑表单项功能
    public array $editControls = []; //自定义编辑表单项
    public string|array $editApi = ''; //配置编辑接口
    public array $editDialog = []; //配置编辑弹框其它属性
    public bool $removable = false; //是否有删除表单项
    public string|array $deleteApi = ''; //配置表单项删除接口
    public string $labelField = 'label'; //标识选项中哪个字段是label值
    public string $valueField = 'value'; //标识选项中哪个字段是value值
    public bool $joinValues = true; //是否拼接value值
    public bool $extractValue = false; //是否将value值抽取出来组成新的数组，只有在joinValues是false是生效
    public int $itemHeight = 32; //每个选项的高度，用于虚拟渲染
    public int $virtualThreshold = 100; //在选项数量超过多少时开启虚拟渲染
    public bool $valuesNoWrap = false; //默认情况下多选所有选项都会显示，通过这个可以最多显示一行，超出的部分变成 ...
    public array $options = [];

}