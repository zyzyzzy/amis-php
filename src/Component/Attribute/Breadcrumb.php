<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Breadcrumb
{
    const TYPE = 'breadcrumb';
    public string $className = '';
    public string $itemClassName = '';  //导航项类名
    public string $separatorClassName = ''; //分割符类名
    public string $dropdownClassName = '';  //下拉菜单类名
    public string $dropdownItemClassName = ''; //下拉菜单项类名
    public string $separator = ''; //分隔符
    public int $labelMaxLength = 16; //最大展示长度
    public string $tooltipPosition = 'top'; //浮窗提示位置
    public array $items = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->itemClassName) || $data['itemClassName'] = $this->itemClassName;
        empty($this->separatorClassName) || $data['separatorClassName'] = $this->separatorClassName;
        empty($this->dropdownClassName) || $data['dropdownClassName'] = $this->dropdownClassName;
        empty($this->dropdownItemClassName) || $data['dropdownItemClassName'] = $this->dropdownItemClassName;
        empty($this->separator) || $data['separator'] = $this->separator;
        empty($this->labelMaxLength) || $this->labelMaxLength === 16 || $data['labelMaxLength'] = $this->labelMaxLength;
        empty($this->tooltipPosition) || $this->tooltipPosition === 'top' || $data['tooltipPosition'] = $this->tooltipPosition;
        $data['items'] = $this->items;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->items[] = $content;
        return $this;
    }
}