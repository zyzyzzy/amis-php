<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Page
{
    const TYPE = 'page';
    public string|array $title = '';
    public string|array $subTitle = '';
    public array $remark = [];
    public array $aside = [];
    public bool $asideResizor = false;
    public int $asideMinWidth = 0;
    public int $asideMaxWidth = 0;
    public bool $asideSticky = true;
    public array $toolbar = [];
    public array|string $body = [];
    public string $className = '';
    public string $toolbarClassName = '';
    public string $bodyClassName = '';
    public string $asideClassName = '';
    public string $headerClassName = '';
    public string|array $initApi = '';
    public bool $initFetch = true;
    public string $initFetchOn = '';
    public int $interval = 3000;
    public bool $silentPolling = false;
    public string $stopAutoRefreshWhen = '';
    public array $pullRefresh = ['disabled'=>true];

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        if(!empty($this->title)){
            $data['title'] = $this->title;
        }
        if(!empty($this->subTitle)){
            $data['subTitle'] = $this->subTitle;
        }
        if(!empty($this->remark)){
            $data['remark'] = $this->remark;
        }
        if(!empty($this->aside)){
            $data['aside'] = $this->aside;
            $data['asideResizor'] = $this->asideResizor;
            $data['asideSticky'] = $this->asideSticky;
            if(!empty($this->asideMinWidth)){
                $data['asideMinWidth'] = $this->asideMinWidth;
            }
            if(!empty($this->asideMaxWidth)){
                $data['asideMaxWidth'] = $this->asideMaxWidth;
            }
            if(!empty($this->asideClassName)){
                $data['asideClassName'] = $this->asideClassName;
            }
        }
        if(!empty($this->toolbar)){
            $data['toolbar'] = $this->toolbar;
            if(!empty($this->toolbarClassName)){
                $data['toolbarClassName'] = $this->toolbarClassName;
            }
        }
        if(!empty($this->body)){
            $data['body'] = $this->body;
            if(!empty($this->bodyClassName)){
                $data['bodyClassName'] = $this->bodyClassName;
            }
        }
        if(!empty($this->className)){
            $data['className'] = $this->className;
        }
        if(!empty($this->headerClassName)){
            $data['headerClassName'] = $this->headerClassName;
        }
        if(!empty($this->initApi)){
            $data['initApi'] = $this->initApi;
            if(empty($this->initFetch)){
                $data['initFetch'] = $this->initFetch;
            }
            if(!empty($this->initFetchOn)){
                $data['initFetchOn'] = $this->initFetchOn;
            }
            if(!empty($this->interval)){
                $data['interval'] = $this->interval;
            }
            if(!empty($this->silentPolling)){
                $data['silentPolling'] = $this->silentPolling;
            }
            if(!empty($this->stopAutoRefreshWhen)){
                $data['stopAutoRefreshWhen'] = $this->stopAutoRefreshWhen;
            }
        }
        if(!empty($this->pullRefresh)){
            $data['pullRefresh'] = $this->pullRefresh;
        }
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}