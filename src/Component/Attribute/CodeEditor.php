<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait CodeEditor
{
    const TYPE = 'editor';
    public string $language = 'javascript'; //css, javascript, html, json, php,java,go,python,sql,
    public function create(): array
    {
        $data = [];
        $data['type'] = 'editor';
        $data['name'] = $this->name;
        $data['language'] = $this->language;
        $data['allowFullscreen'] = true;
        empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {

    }
}