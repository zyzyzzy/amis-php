<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Group
{
    const TYPE = 'group';
    public string $className = '';
    public string $label = '';
    public string|array $body = [];
    public string $mode = '';
    public string $gap = '';
    public string $direction = 'horizontal';
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->body) || $data['body'] = $this->body;
        empty($this->mode) || $data['mode'] = $this->mode;
        empty($this->gap) || $data['gap'] = $this->gap;
        empty($this->direction) || $this->direction === 'horizontal' || $data['direction'] = 'vertical';
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}