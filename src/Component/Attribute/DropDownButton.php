<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait DropDownButton
{
    const TYPE = 'dropdown-button';
    public string $label = '';
    public string $className = '';
    public string $level = '';
    public string $tooltip = '';
    public string $btnClassName = '';  //按钮 CSS 类名
    public string $menuClassName = ''; //下拉菜单 CSS 类名
    public bool $block = false; //块状显示, 与父类的宽度一样
    public string $size = '';
    public string $align = 'left'; //下拉框位置 默认 left 可选 right
    public bool $iconOnly = false; //是否只显示图标
    public bool $defaultIsOpened = false; //是否默认打开
    public bool $closeOnOutside = true;  //点击外侧区域是否收起
    public bool $closeOnClick = false; //点击按钮后自动关闭下拉菜单
    public string $trigger = 'click'; //触发方式 点击 click 鼠标悬浮 hover
    public bool $hideCaret = false; //隐藏下拉图标
    public string $icon = ''; //左侧图标
    public string $rightIcon = ''; //右侧图标
    public array $buttons = []; //配置下拉图标

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['label'] = $this->label;
        empty($this->level) || $data['level'] = $this->level;
        empty($this->tooltip) || $data['tooltip'] = $this->tooltip;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->btnClassName) || $data['btnClassName'] = $this->btnClassName;
        empty($this->menuClassName) || $data['menuClassName'] = $this->menuClassName;
        $this->block === false || $data['block'] = true;
        empty($this->size) || $data['size'] = $this->size;
        empty($this->align) || $this->align === 'left' || $data['align'] = $this->align;
        empty($this->icon) || $data['icon'] = $this->icon;
        empty($this->rightIcon) || $data['rightIcon'] = $this->rightIcon;
        $this->iconOnly === false || $data['iconOnly'] = true;
        $this->defaultIsOpened === false || $data['defaultIsOpened'] = true;
        $this->closeOnOutside === true || $data['closeOnOutside'] = false;
        $this->closeOnClick === false || $data['closeOnClick'] = true;
        empty($this->trigger) || $this->trigger === 'click' || $data['trigger'] = $this->trigger;
        $this->hideCaret === false || $data['hideCaret'] = $this->hideCaret;
        $data['buttons'] = $this->buttons;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->buttons[] = $content;
        return $this;
    }
}