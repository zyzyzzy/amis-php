<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait ListSelect
{
    const TYPE = 'list-select';
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->source) || $data['source'] = $this->source;
        $this->multiple === false || $data['multiple'] = true;
        empty($this->options) || $data['options'] = $this->options;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        $this->options[] = $content;
        return $this;
    }
}