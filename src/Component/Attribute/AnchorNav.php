<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait AnchorNav
{
    const TYPE = 'anchor-nav';
    public string $className = '';
    public string $linkClassName = '';  //导航 Dom 的类名
    public string $sectionClassName = ''; //锚点区域 Dom 的类名
    public array $links = [];
    public string $direction = 'vertical';  //可以配置导航水平展示还是垂直展示。对应的配置项分别是：vertical、horizontal
    public string|int $active = ''; //需要定位的区域

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->linkClassName) || $data['linkClassName'] = $this->linkClassName;
        empty($this->sectionClassName) || $data['sectionClassName'] = $this->sectionClassName;
        empty($this->direction) || $this->direction === 'vertical' || $data['direction'] = 'horizontal';
        $this->active === '' || $data['active'] = $this->active;
        $data['links'] = $this->links;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->links[] = $content;
        return $this;
    }
}