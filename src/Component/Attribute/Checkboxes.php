<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Checkboxes
{
    const TYPE = 'checkboxes';
//    private string|array $source = '';
    public bool $checkAll = false; //是否支持全选
    public bool $defaultCheckAll = false; //defaultCheckAll
    public string $menuTpl = ''; // 支持自定义选项渲染
//    private bool $inline = true; //是否显示一行
    public int|array $columnsCount = 1; //选项按几列显示，默认为一列
    public string $itemClassName = ''; //选项样式类名
//    private string $labelClassName = ''; //选项标签样式类名
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->source) || $data['source'] = $this->source;
        if($this->inline === false){
            $data['inline'] = false;
            empty($this->columnsCount) || $this->columnsCount === 1 || $data['columnsCount'] = $this->columnsCount;
        }
        empty($this->menuTpl) || $data['menuTpl'] = $this->menuTpl;
        $this->checkAll === false || $data['checkAll'] = true;
        $this->defaultCheckAll === false || $data['defaultCheckAll'] = true;
        empty($this->itemClassName) || $data['itemClassName'] = $this->itemClassName;
        empty($this->labelClassName) || $data['labelClassName'] = $this->labelClassName;
        empty($this->options) || $data['options'] = $this->options;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {

    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}