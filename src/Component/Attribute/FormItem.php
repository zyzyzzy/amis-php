<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait FormItem
{
    public string $className = ''; //表单最外层类名
    public string $inputClassName = ''; //表单控制器类名
    public string $labelClassName = ''; //label的类名
    public string|array $value = ''; //表单默认值
    public string|bool $label = ''; //表单项标签
    public string $labelAlign = 'left'; //表单项标签对齐方式，默认右对齐，仅在 mode为horizontal 时生效 left|right
    public string $labelRemark = ''; //表单项标签描述
    public string $size = ''; //表单尺寸大小
    public bool $static = false; //表单项静态展示
    public string $staticOn = ''; //表达式配置静态展示
    public bool $hidden = false; //表单项是否隐藏
    public string $hiddenOn = ''; //表达式来配置表单项是否隐藏
    public bool $visible = true; //表单项是否可见
    public string $visibleOn = ''; //表达式来配置表单项是否可见
    public bool $clearValueOnHidden = false; //表单项隐藏时是否删除该表单项对应的值
    public array $staticSchema = []; //自定义静态展示方式
    public string $placeholder = ''; //表单项描述(内部)
    public string $description = ''; //表单项描述(底部)
    public bool $inline = false; //是否为 内联 模式
    public bool $submitOnChange = false; // 是否该表单项值发生变化时就提交当前表单。
    public bool $disabled = false; //当前表单项是否是禁用状态
    public string $disabledOn = ''; //当前表单项是否禁用的条件
    public bool $required = false; //是否为必填。
    public string $requiredOn = ''; //通过表达式来配置当前表单项是否为必填。
    public bool $validateOnChange = false; // 表单值发生变化时就校验
    public array|string $validations = ''; //表单项值格式验证，支持设置多个，多个规则用英文逗号隔开。
    /**
     * 单独表单项校验接口
     *  [
     *      'status' => 422,
     *      'errors' => '错误内容'
     *  ]
     * status: 返回 0 表示校验成功，422 表示校验失败;
     * errors: 返回 status 为 422 时，显示的校验失败信息;

     */
    public string $validateApi = ''; //单独表单项校验接口 返回



}