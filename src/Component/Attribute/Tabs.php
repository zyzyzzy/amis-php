<?php

namespace Aimanong\AmisPhp\Component\Attribute;
use Aimanong\AmisPhp\Component\Ability\Service;

trait Tabs
{
    const TYPE = 'tabs';

    //组件初始化时激活的选项卡，hash 值或索引值，支持使用表达式 2.7.1 以上版本
    public string|int $defaultKey = '';
    //激活的选项卡，hash 值或索引值，支持使用表达式，可响应上下文数据变化
    public string|int $activeKey = '';
    public string $className = '';
    //展示模式，取值可以是 line、card、radio、vertical、chrome、simple、strong、tiled、sidebar
    public string $tabsMode = '';
    public string $tabsClassName = '';  //Tabs Dom 的类名
    public string|array $source = ''; //原amis里 这个参数是一个变量名,amis-php里这里是一个api地址
    public string $sourceVariableName = '$tabsData'; //source变量名
    public array $toolbar = []; //tabs 中的工具栏
    public string $toolbarClassName = ''; //tabs 中工具栏的类名
    public bool $mountOnEnter = false; //只有在点中 tab 的时候才渲染
    public bool $unmountOnExit = false; //切换 tab 的时候销毁
    public bool $addable = false; //是否支持新增
    public string $addBtnText = '增加'; //新增按钮文案
    public bool $closable = false; //是否支持删除
    public bool $draggable = false; //是否支持拖拽
    public bool $showTip = false; //是否支持提示
    public string $showTipClassName = ''; //提示的类
    public bool $editable = false; //是否可编辑标签名
    public string $sidePosition = 'left'; //sidebar 模式下，标签栏位置 left|right
    public int $collapseOnExceed = 0; //当 tabs 超出多少个时开始折叠
    public string $collapseBtnLabel = 'more'; //用来设置折叠按钮的文字
    public array $tabs = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $this->defaultKey === '' || $data['defaultKey'] = $this->defaultKey;
        $this->activeKey === '' || $data['activeKey'] = $this->activeKey;
        empty($this->className) || $data['className'] = $this->className;
        $data['tabsMode'] = $this->tabsMode?:'line';
        $this->tabsMode === 'sidebar' && (empty($this->sidePosition) || $this->sidePosition === 'left' || $data['sidePosition'] = $this->sidePosition);
        empty($this->tabsClassName) || $data['tabsClassName'] = $this->tabsClassName;
//        empty($this->source) || $data['source'] = $this->source;
        empty($this->toolbar) || $data['toolbar'] = $this->toolbar;
        empty($this->toolbarClassName) || $data['toolbarClassName'] = $this->toolbarClassName;
        empty($this->addable) || $data['addable'] = $this->addable;
        empty($this->addBtnText) || $this->addBtnText === '增加' || $data['addBtnText'] = $this->addBtnText;
        empty($this->closable) || $data['closable'] = $this->closable;
        empty($this->draggable) || $data['draggable'] = $this->draggable;
        empty($this->showTip) || $data['showTip'] = $this->showTip;
        empty($this->showTipClassName) || $data['showTipClassName'] = $this->showTipClassName;
        empty($this->editable) || $data['editable'] = $this->editable;
        empty($this->collapseOnExceed) || $data['collapseOnExceed'] = $this->collapseOnExceed;
        empty($this->collapseBtnLabel) || $this->collapseBtnLabel === 'more' || $data['collapseBtnLabel'] = $this->collapseBtnLabel;
        empty($this->mountOnEnter) || $data['mountOnEnter'] = $this->mountOnEnter;
        empty($this->unmountOnExit) || $data['unmountOnExit'] = $this->unmountOnExit;
        $data['tabs'] = $this->tabs;
        //如果source不为空时,用service把tabs包裹起来,并把source赋值给service的api
        if(!empty($this->source)){
            $data['source'] = '$'.($this->sourceVariableName?:'tabsData');
            $service = new Service(api:$this->source);
            $service->appendBody($data);
            $data = $service->create();
        }
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->tabs[] = $content;
        return $this;
    }
}