<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait CRUD
{
    const TYPE = 'crud';
    public string|array $api = ''; //数据源接口
    public string|array $quickSaveApi = ''; //快速编辑保存接口
    public string|array $quickSaveItemApi = ''; //快速编辑即时保存接口
    public array $columns = []; //展示例
    public array $filter = [];
    public array $autoGenerateFilter = []; //自动生成查询区域
    public array $defaultParams = []; //默认请求参数
    public int $interval = 0; //数据源接口轮询时间
    public string $stopAutoRefreshWhen = ''; //停止轮询的表达式
    public string $primaryField = 'id'; //	设置 ID 字段名。批量修改和删除时会用到
    public array $headerToolbar = [
        'filter-toggler',
        'bulkActions',
        'reload',
        [
            "type" => "columns-toggler",
            "align" => "right",
            "draggable" => true,
            "icon" => "fas fa-cog",
            "overlay" => true,
            "footerBtnSize" => "sm"
        ]
    ]; //头部配置
    public array $bulkActions = []; //批量处理按钮
    public array $footerToolbar = [
        'switch-per-page', //分页每页显示多少行的下拉选择框
        'statistics',
        'pagination'
    ]; //底部配置
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->api) || $data['api'] = $this->api;
        if(!empty($this->autoGenerateFilter)){
            $data['autoGenerateFilter'] = $this->autoGenerateFilter;
        }else{
            empty($this->filter) || $data['filter'] = $this->filter;
        }
        empty($this->defaultParams) || $data['defaultParams'] = $this->defaultParams;
        empty($this->interval) || $this->interval === 3000 || $data['interval'] = $this->interval;
        empty($this->stopAutoRefreshWhen) || $data['stopAutoRefreshWhen'] = $this->stopAutoRefreshWhen;
        empty($this->quickSaveApi) || $data['quickSaveApi'] = $this->quickSaveApi;
        empty($this->quickSaveItemApi) || $data['quickSaveItemApi'] = $this->quickSaveItemApi;
        empty($this->columns) || $data['columns'] = $this->columns;
        empty($this->primaryField) || $this->primaryField === 'id' || $data['primaryField'] = $this->primaryField;
        $data['headerToolbar'] = $this->headerToolbar;
        $data['footerToolbar'] = $this->footerToolbar;
        empty($this->bulkActions) || $data['bulkActions'] = $this->bulkActions;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}