<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputRange
{
    const TYPE = 'input-range';
    public bool $multiple = false ; //是否支持多选
    public int|float $min = 0; //最小值
    public int|float $max = 0; //最大值
    public int|float $step = 1; //步长(粒度)
    public bool $showSteps = false; //显示步长
    public int $parts = 1; //分割块数
    public array $marks = []; //刻度标记
    public bool $showInput = false; //是否显示输入框
    public bool $clearable = false; //是否可清除 前置条件：开启showInput时有效
    public bool $tooltipVisible = false; //是否显示滑块标签
    public string $tooltipPlacement = 'top';  //滑块标签的位置，默认auto，方向自适应 前置条件：tooltipVisible 不为 false 时有效
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        $this->multiple === false || $data['multiple'] = true;
        $data['min'] = $this->min;
        empty($this->max) || $data['max'] = $this->max;
        empty($this->step) || $this->step === 1 || $data['step'] = $this->step;
        $this->disabled === false || $data['disabled'] = true;
        $this->showSteps === false || $data['showSteps'] = true;
        empty($this->parts) || $this->parts === 1 || $data['parts'] = $this->parts;
        $this->showInput === false || $data['showInput'] = true;
        $this->showInput === false || $data['clearable'] = true;
        $this->clearable === false || $data['clearable'] = true;
        $this->tooltipVisible === false || $data['tooltipVisible'] = true;
        $this->tooltipVisible === false || $this->tooltipPlacement === 'top' || $data['tooltipPlacement'] = $this->tooltipPlacement;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {

    }
}