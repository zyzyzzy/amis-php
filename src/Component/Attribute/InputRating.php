<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputRating
{
    const TYPE = 'input-rating';
    public bool $half = true; //是否使用半星选择
    public int $count = 0; //总星数
    public bool $readOnly = false; //是否只读
    public bool $allowClear = true; //是否允许再次点击后清除
    public array|string $colors = []; //星星被选中的颜色。 若传入字符串，则只有一种颜色。若传入对象，可自定义分段，键名为分段的界限值，键值为对应的类名
    public string $inactiveColor = '#e7e7e8'; //未被选中的星星的颜色
    public array|string $texts = []; //星星被选中时的提示文字。可自定义分段，键名为分段的界限值，键值为对应的类名
    public string $textPosition  ='left'; //文字的位置 left|right
    public string $char = '★'; //自定义字符
    public string $charClassName = '';
    public string $textClassName = '';
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        $this->half === false || $data['half'] = true;
        $data['count'] = $this->count;
        $this->readOnly === false || $data['readOnly'] = true;
        $this->allowClear === true || $data['allowClear'] = false;
        empty($this->inactiveColor) || $this->inactiveColor === '#e7e7e8' || $data['inactiveColor'] = $this->inactiveColor;
        empty($this->colors) || $data['colors'] = $this->colors;
        empty($this->texts) || $data['texts'] = $this->texts;
        empty($this->textPosition) || $this->textPosition === 'left' || $data['textPosition'] = $this->textPosition;
        empty($this->char) || $this->char === '★' || $data['char'] = $this->char;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->charClassName) || $data['charClassName'] = $this->charClassName;
        empty($this->textClassName) || $data['textClassName'] = $this->textClassName;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}