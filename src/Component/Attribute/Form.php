<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Form
{
    const TYPE = 'form';
    public bool $debug = false;
    public string $mode = 'normal'; //表单展示方式，可以是：normal(默认)、horizontal(水平) 或者 inline(内联)
    public array $horizontal = [
        'left' => 2,
        'right' => 10,
        'justify' => false
    ];
    public string $labelAlign = 'right'; //表单项标签对齐方式，默认右对齐，仅在 mode为horizontal 时生效  left|right
    public string|int $labelWidth = ''; //表单项标签自定义宽度 如果为数字单位默认为px
    public string|array $title = 'amis-php 表单'; //Form 的标题
    public string $submitText = '提交'; //默认的提交按钮名称，如果设置成空，则可以把默认按钮去掉。
    public string $className = '';
    public array $body = [];
    public array|string $actions = ''; //Form 提交按钮，成员为 Action 设置为[],可以把默认按钮去掉
    public bool $wrapWithPanel = true; //是否让 Form 用 panel 包起来(Form是否有外框)，设置为 false 后，actions 将无效。
    public string $panelClassName = ''; // 外层 panel 的类名
    public string|array $api = ''; //Form 用来保存数据的 api
    public string|array $initApi = ''; //Form 用来获取初始数据的 api
    public int $interval = 3000; //刷新时间(最低 3000) 单位是 毫秒
    public bool $silentPolling = false; //配置刷新时是否显示加载动画
    public string $stopAutoRefreshWhen = ''; //通过 表达式 来配置停止刷新的条件
    public string|array $initAsyncApi = ''; //Form 用来获取初始数据的 api,与 initApi 不同的是，会一直轮询请求该接口，直到返回 finished 属性为 true 才 结束。
    public bool $initFetch = true; //设置了 initApi 或者 initAsyncApi 后，默认会开始就发请求，设置为 false 后就不会起始就请求接口
    public string $initFetchOn = ''; //设置了 initApi 或者 initAsyncApi 后,通过 表达式 来配置开始发请求
    public string $initFinishedField = ''; //设置了 initAsyncApi 后，默认会从返回数据的 data.finished 来判断是否完成，也可以设置成其他的 xxx，就会从 data.xxx 中获取
    public int $initCheckInterval = 3000; //设置了 initAsyncApi 以后，默认拉取的时间间隔 单位毫秒
    public string|array $asyncApi = ''; //设置此属性后，表单提交发送保存接口后，还会继续轮询请求该接口，直到返回 finished 属性为 true 才 结束。
    public int $checkInterval = 3000; //轮询请求的时间间隔，默认为 3 秒。设置 asyncApi 才有效 单位 毫秒
    public string $finishedField = 'finished'; //如果决定结束的字段名不是 finished 请设置此属性，比如 is_success 设置 asyncApi 才有效
    public bool $submitOnChange = false; //表单修改即提交
    public bool $submitOnInit = false; //初始就提交一次
    public bool $resetAfterSubmit = false; //提交后是否重置表单
    public string $primaryField = 'id'; // 设置emp主键 id, 当设置后，检测表单是否完成时（asyncApi），只会携带此数据。

    /**
     * 默认表单提交自己会通过发送 api 保存数据，但是也可以设定另外一个 form 的 name 值，
     * 或者另外一个 CRUD 模型的 name 值。 如果 target 目标是一个 Form ，
     * 则目标 Form 会重新触发 initApi，api 可以拿到当前 form 数据。
     * 如果目标是一个 CRUD 模型，则目标模型会重新触发搜索，参数为当前 Form 数据。
     * 当目标是 window 时，会把当前表单的数据附带到页面地址上。
     */
    public string $target = '';
    public string $redirect = ''; //设置此属性后，Form 保存成功后，自动跳转到指定页面。支持相对地址，和绝对地址（相对于组内的）。
    public string $reload = ''; //操作完后刷新目标对象。请填写目标组件设置的 name 值，如果填写为 window 则让当前页面整体刷新。
    public bool $autoFocus = false; //是否自动聚焦。
    public bool $canAccessSuperData = true; //指定是否可以自动获取上层的数据并映射到表单项上
    public string $persistData = ''; //指定一个唯一的 key，来配置当前表单是否开启本地缓存
    public bool $clearPersistDataAfterSubmit = true; //指定表单提交成功后是否清除本地缓存
    public bool $preventEnterSubmit = false; //禁用回车提交表单
    public bool $trimValues = false; //是否删除每个表单项值的前后空格
    public bool $promptPageLeave = false; //form 还没保存，即将离开页面前是否弹框确认。
    public int $columnCount = 0; //表单项显示为几列

    /**
     * 默认表单是采用数据链的形式创建个自己的数据域，表单提交的时候只会发送自己这个数据域的数据，
     * 如果希望共用上层数据域可以设置这个属性为 false，这样上层数据域的数据不需要在表单中用隐藏域或者显式映射才能发送了
     */
    public bool $inheritData = true;
    public bool $static = false; // 整个表单静态方式展示
    public string $staticClassName = ''; //表单静态展示时使用的类名
    public array $rules = []; //表单验证

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        $data['mode'] = $this->mode;
        if($this->mode === 'horizontal'){
            $data['horizontal'] = $this->horizontal;
        }
        empty($this->labelAlign) || $this->labelAlign === 'right' ||$data['labelAlign'] = $this->labelAlign;
        empty($this->labelWidth) || $data['labelWidth'] = $this->labelWidth;
        $data['title'] = $this->title;
        $data['submitText'] = $this->submitText;
        empty($this->className) || $data['className'] = $this->className;
        $data['body'] = $this->body;
        $this->actions === '' || $data['actions'] = $this->actions;
        $this->wrapWithPanel === true || $data['wrapWithPanel'] = $this->wrapWithPanel;
        empty($this->panelClassName) || $data['panelClassName'] = $this->panelClassName;
        empty($this->api) || $data['api'] = $this->api;
        empty($this->initApi) || $data['initApi'] = $this->initApi;
        empty($this->interval) || $this->interval === 3000 || $data['interval'] = $this->interval;
        $this->silentPolling === false || $data['silentPolling'] = true;
        empty($this->stopAutoRefreshWhen) || $data['stopAutoRefreshWhen'] = $this->stopAutoRefreshWhen;
        empty($this->initAsyncApi) || $data['initAsyncApi'] = $this->initAsyncApi;
        $this->initFetch === true || $data['initFetch'] = false;
        empty($this->initFetchOn) || $data['initFetchOn'] = $this->initFetchOn;
        empty($this->initFinishedField) || $data['initFinishedField'] = $this->initFinishedField;
        empty($this->initCheckInterval) || $this->initCheckInterval === 3000 || $data['initCheckInterval'] = $this->initCheckInterval;
        empty($this->asyncApi) || $data ['asyncApi'] = $this->asyncApi;
        empty($this->checkInterval) || $this->checkInterval === 3000 || $data['checkInterval'] = $this->checkInterval;
        empty($this->finishedField) || $data['finishedField'] = $this->finishedField;
        $this->submitOnChange === false || $data['submitOnChange'] = true;
        $this->submitOnInit === false || $data['submitOnInit'] = true;
        $this->resetAfterSubmit === false || $data['resetAfterSubmit'] = true;
        empty($this->primaryField) || $this->primaryField === 'id' || $data['primaryField'] = $this->primaryField;
        empty($this->target) || $data['target'] = $this->target;
        empty($this->redirect) || $data['redirect'] = $this->redirect;
        empty($this->reload) || $data['reload'] = $this->reload;
        $this->autoFocus === false || $data['autoFocus'] = true;
        $this->canAccessSuperData === true || $data['canAccessSuperData'] = false;
        empty($this->persistData) || $data['persistData'] = $this->persistData;
        $this->clearPersistDataAfterSubmit === true || $data['clearPersistDataAfterSubmit'] = false;
        $this->preventEnterSubmit === false || $data['preventEnterSubmit'] = $this->preventEnterSubmit;
        $this->trimValues === false || $data['trimValues'] = true;
        $this->promptPageLeave === false || $data['promptPageLeave'] = true;
        empty($this->columnCount) || $data['columnCount'] = $this->columnCount;
        empty($this->rules) || $data['rules'] = $this->rules;
        $this->debug === false || $data['debug'] = true;
        empty($this->static) || $data['static'] = true;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}