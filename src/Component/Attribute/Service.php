<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Service
{
    const TYPE = 'service';
    public string $className = '';
    public array $body = [];
    public string|array $api = ''; //初始化数据域接口地址
    public string $ws = '';  //WebScocket地址
    public string|array $schemaApi = ''; //用户来获取远程Schema接口地址
    public int $interval = 0; //轮询时间,单位毫秒, 为 0 时不轮询
    public bool $silentPolling = true; //配置轮询时是否显示加载动画
    public string $stopAutoRefreshWhen = '';  //配置停止轮询的条件

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['className'] = $this->className;
        $data['body'] = $this->body;
        empty($this->api) || $data['api'] = $this->api;
        empty($this->ws) || $data['ws'] = $this->ws;
        empty($this->schemaApi) || $data['schemaApi'] = $this->schemaApi;
        $this->interval === 0 || $data['interval'] = $this->interval || empty($this->silentPolling) || $data['silentPolling'] = true;
        empty($this->stopAutoRefreshWhen) || $data['stopAutoRefreshWhen'] = $this->stopAutoRefreshWhen;
        return $data;

    }

    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}