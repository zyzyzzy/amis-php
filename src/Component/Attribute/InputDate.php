<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputDate
{
    const TYPE = 'input-date';
//    public string $value = ''; //默认值, 时间戳
//    public string $label = '';
    public string $format = 'YYYY-MM-DD'; //时间值格式
    public string $inputFormat = 'YYYY-MM-DD';  //时间显示格式
    public bool $closeOnSelect = true; //点选日期后，是否马上关闭选择框
    public string $placeholder = ''; //占位提示文本,在输入框内
    public string $description = ''; //输入框下面的提示文本
    public string $minDate = ''; //限制最小日期 时间戳
    public string $maxDate = ''; //限制最大日期 时间戳
    public bool $utc = false; //保存UTC时间值
    public bool $clearable = true; //是否可以清除
    public bool $embed = false; // 是否内联模式
    public array $shortcuts = []; //日期快捷键
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        $data['label'] = $this->label;
        empty($this->value) || $data['value'] = $this->value;
        empty($this->format) || $data['format'] = $this->format;
        empty($this->inputFormat) || $data['inputFormat'] = $this->inputFormat;
        $this->closeOnSelect === false || $data['closeOnSelect'] = true;
        empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
        empty($this->description) || $data['description'] = $this->description;
        empty($this->minDate) || $data['minDate'] = $this->minDate;
        empty($this->maxDate) || $data['maxDate'] = $this->maxDate;
        $this->utc === false || $data['utc'] = true;
        $this->clearable == true || $data['clearable'] = false;
        $this->embed === false || $data['embed'] = true;
        empty($this->shortcuts) || $data['shortcuts'] = $this->shortcuts;
        return $data;

    }

    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}