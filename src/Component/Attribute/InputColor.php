<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputColor
{
    const TYPE = 'input-color';
    public array $presetColors = []; //选择器底部的默认颜色，数组内为空则不显示默认颜色
    public bool $allowCustomColor = true; //为false时只能选择颜色，使用 presetColors 设定颜色选择范围
    public bool $clearable = true; //是否显示清除按钮
    public string $resetValue = ''; //清除后，表单项值调整成该值
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->presetColors) || $data['presetColors'] = $this->presetColors;
        $this->allowCustomColor === true || $data['allowCustomColor'] = false;
        $this->clearable === true || $data['clearable'] = false;
        empty($this->resetValue) || $data['resetValue'] = $this->resetValue;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {

    }
}