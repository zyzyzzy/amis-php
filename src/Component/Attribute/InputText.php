<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputText
{
    const TYPE = 'input-text';
    public string $borderMode = 'full'; //输入框边框模式，全边框，还是半边框，或者没边框。full|half|none
    public bool $clearable = false; //是否可清除
    public bool $trimContents = false; //是否去除首尾空白文本。
    public string $prefix = '';  //前缀
    public string $suffix = ''; //后缀
    public int $showCounter = 0; //显示计数器
    public int $minLength = 0; //最小可输入字符串长度
    public int $maxLength = 0; //最大可输入字符串长度
    public bool $clearValueOnEmpty = false; //当输入框的值清空时，提交的表单项里就不会有这个值
    public string $transform = ''; //自动转换值，支持转小写或大写。lowerCase|upperCase
    public array $addOn = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        (empty($this->label) && $this->label !== false) || $data['label'] = $this->label;
        empty($this->borderMode) || $this->borderMode === 'full' || $data['borderMode'] = $this->borderMode;
        $this->clearable === false || $data['clearable'] = true;
        $this->trimContents === false || $data['trimContents'] = true;
        empty($this->showCounter) || $data['showCounter'] = $this->showCounter;
        empty($this->showCounter) || empty($this->maxLength) || $data['maxLength'] = $this->maxLength;
        empty($this->minLength) || $data['minLength'] = $this->minLength;
        $this->clearValueOnEmpty === false || $data['clearValueOnEmpty'] = true;
        empty($this->transform) || $data['transform'][$this->transform] = true;
        empty($this->value) || $data['value'] = $this->value;
        empty($this->prefix) || $data['prefix'] = $this->prefix;
        empty($this->suffix) || $data['suffix'] = $this->suffix;
        empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
        empty($this->description) || $data['description'] = $this->description;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->addOn) || $data['addOn'] = $this->addOn;
        empty($this->size) || $data['size'] = $this->size;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}