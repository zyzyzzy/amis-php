<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Select
{
    const TYPE = 'select';
    //可选：group、table、tree、chained、associated。
    //分别为：列表形式、表格形式、树形选择形式、级联选择形式，关联选择形式（与级联选择的区别在于，级联是无限极，而关联只有一级，关联左边可以是个 tree）
    public string $selectMode = '';
    public array $columns = []; //表格形式时的表头
    public bool $multiple = false; //是否多选
    public bool $searchable = false; //是否可以搜索
    public int $maxTagCount = 0; //标签的最大展示数量，超出数量后以收纳浮层的方式展示，仅在多选模式开启后生效
    
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
        empty($this->description) || $data['description'] = $this->description;
        empty($this->className) || $data['className'] = $this->className;
        (empty($this->label) && $this->label !== false) || $data['label'] = $this->label;
        empty($this->multiple) || $data['multiple'] = true;
        empty($this->searchable) || $data['searchable'] = true;
        empty($this->source) || $data['source'] = $this->source;
        empty($this->maxTagCount) || $data['maxTagCount'] = $this->maxTagCount;

        empty($this->creatable) || $data['creatable'] = true;
        empty($this->addControls) || $data['addControls'] = $this->addControls;
        empty($this->addApi) || $data['addApi'] = $this->addApi;

        empty($this->editable) || $data['editable'] = true;
        empty($this->editControls) || $data['editControls'] = $this->editControls;
        empty($this->editApi) || $data['editApi'] = $this->editApi;

        empty($this->optionLabel) || $data['optionLabel'] = $this->optionLabel;

        empty($this->removable) || $data['removable'] = true;
        empty($this->deleteApi) || $data['deleteApi'] = $this->deleteApi;

        $data['options'] = $this->options;

        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}