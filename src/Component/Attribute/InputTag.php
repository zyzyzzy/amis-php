<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputTag
{
    const TYPE = 'input-tag';
    public bool $clearable = false; //在有值的时候是否显示一个删除图标在右侧。
    public string $resetValue = ''; //删除后设置此配置项给定的值。
    public int $max = 0; //允许添加的标签的最大数量
    public int $maxTagLength = 0; //单个标签的最大文本长度
    public int $maxTagCount = 0; //标签的最大展示数量，超出数量后以收纳浮层的方式展示，仅在多选模式开启后生效
    public array $overflowTagPopover = []; //收纳浮层的配置属性，详细配置参考Tooltip
    public bool $enableBatchAdd = false; //是否开启批量添加模式
    public string $separator = ''; //开启批量添加后，输入多个标签的分隔符，支持传入多个符号，默认为"-"
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
        empty($this->source) || $data['source'] = $this->source;
        $this->clearable === false || $data['clearable'] = true;
        empty($this->resetValue) || $data['resetValue'] = $this->resetValue;
        empty($this->max) || $data['max'] = $this->max;
        empty($this->maxTagLength) || $data['maxTagLength'] = $this->maxTagLength;
        empty($this->maxTagCount) || $data['maxTagCount'] = $this->maxTagCount;
        empty($this->overflowTagPopover) || $data['overflowTagPopover'] = $this->overflowTagPopover;
        $this->enableBatchAdd === false || $data['enableBatchAdd'] = true;
        empty($this->separator) || $data['separator'] = $this->separator;
        empty($this->options) || $data['options'] = $this->options;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->options[] = $content;
        return $this;
    }
}