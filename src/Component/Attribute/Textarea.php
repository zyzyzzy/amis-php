<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Textarea
{
    const TYPE = 'textarea';
    public int $minRows = 3; //最小行数
    public int $maxRows = 20; //最大行数
    public bool $trimContents  = true; //是否去除首尾空白文本
    public bool $readOnly = false; //是否只读
    public bool $showCounter = false; //是否显示计数器
    public int $maxLength = 0; //限制最大字数
    public bool $clearable = false; //是否可以清除
    public string $resetValue = ''; //清除后设置此配置项给定的值。
    public function create(): array
    {
        $data =  [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->minRows) || $this->minRows === 3 || $data['minRows'] = $this->minRows;
        empty($this->maxRows) || $this->maxRows === 20 || $data['maxRows'] = $this->maxRows;
        $this->trimContents === true || $data['trimContents'] = false;
        $this->readOnly === false || $data['readOnly'] = true;
        $this->showCounter === false || $data['showCounter'] = true;
        empty($this->maxLength) || $data['maxLength'] = $this->maxLength;
        $this->clearable === false || $data['clearable'] = true;
        empty($this->resetValue) || $data['resetValue'] = $this->resetValue;
        return $data;

    }
    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}