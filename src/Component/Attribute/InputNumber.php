<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputNumber
{
    const TYPE = 'input-number';
    public int $min = 0; //最小值
    public int $max = 0; //最大值
    public float $step = 0; //步长 比如 0.001
    public int $precision = 0; //精度，即小数点后几位，支持 0 和正整数
    public bool $showSteps = false;//是否显示上下点击按钮
    public string $prefix = ''; //前缀
    public string $suffix = ''; //后缀
    public bool $kilobitSeparator = false; //千分分隔
    public bool $keyboard = false; //键盘事件（方向上下）
    public bool $big = false; //是否使用大数
    public string $displayMode = ''; //enhance 加强版输入框
    public string|int $resetValue = ''; //清空输入内容时，组件值将设置为resetValue
    public bool $clearValueOnEmpty = false; //内容为空时从数据域中删除该表单项对应的值
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        $data['value'] = $this->value;
        empty($this->label) || $data['label'] = $this->label;
        empty($this->min) || $data['min'] = $this->min;
        empty($this->max) || $data['max'] = $this->max;
        empty($this->step) || $data['step'] = $this->step;
        empty($this->precision) || $data['precision'] = $this->precision;
        $this->showSteps === false || $data['showSteps'] = true;
        empty($this->prefix) || $data['prefix'] = $this->prefix;
        empty($this->suffix) || $data['suffix'] = $this->suffix;
        $this->kilobitSeparator === false || $data['kilobitSeparator'] = true;
        $this->keyboard === false || $data['keyboard'] = true;
        $this->big === false || $data['big'] = true;
        empty($this->displayMode) || $data['displayMode'] = 'enhance';
        empty($this->resetValue) || $data['resetValue'] = $this->resetValue;
        $this->clearValueOnEmpty === false || $data['clearValueOnEmpty'] = true;
        return $data;

    }

    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}