<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Flex
{
    const TYPE = 'flex';
    public string $className = '';
    /**
     * 子节点水平分布 center(水平居中)、flex-start(靠头部左边)、flex-end(靠尾部右边)、space-around(环绕)、space-between(间距)、space-evenly(平均)
     * @var string
     */
    public string $justify = 'center';

    /**
     * 垂直方向 center、flex-start、flex-end
     * @var string
     */
    public string $alignItems = 'center';
    public array $items = [];
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        if(!empty($this->justify) && $this->justify != 'center'){
            $data['justify'] = $this->justify;
        }
        if(!empty($this->alignItems) && $this->alignItems != 'center'){
            $data['alignItems'] = $this->alignItems;
        }
        $data['items'] = $this->items;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->items[] = $content;
        return $this;
    }
}