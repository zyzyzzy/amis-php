<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputGroup
{
    const TYPE = 'input-group';
    public string $className = '';
    public string|array $body = [];
    public function create()
    {
        $data = [];
        $data['type'] = 'input-group';
        empty($this->className) || $data['className'] = $this->className;
        $data['body'] = $this->body;
        return $data;
    }
    public function appendBody(string|array $content = ''): static
    {
        $this->body[] = $content;
        return $this;
    }
}