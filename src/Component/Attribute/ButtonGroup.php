<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait ButtonGroup
{
    const TYPE = 'button-group';
    public bool $vertical = false; //是否垂直模式
    public bool $tiled = false; //是否平铺模式
    public string $btnLevel = '';  //配置 btnLevel 统一设置按钮主题样式
    public string $btnActiveLevel = '';  //为按钮设置激活态时的主题样式
    public array $buttons = [];
    public string $className = '';

    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $this->vertical === false || $data['vertical'] = true;
        $this->tiled === false || $data['tiled'] = true;
        empty($this->btnLevel) || $data['btnLevel'] = $this->btnLevel;
        empty($this->btnActiveLevel) || $data['btnActiveLevel'] = $this->btnActiveLevel;
        $data['buttons'] = $this->buttons;
        empty($this->className) || $data['className'] = $this->className;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->buttons[] = $content;
        return $this;
    }
}