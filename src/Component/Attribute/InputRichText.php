<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait InputRichText
{
    const TYPE = 'input-rich-text';
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $data['name'] = $this->name;
        empty($this->label) || $data['label'] = $this->label;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}