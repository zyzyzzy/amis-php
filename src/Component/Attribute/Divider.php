<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait Divider
{
    const TYPE = 'divider';
    /**
     * 虚线(dashed)还是实线(solid)
     * @var string
     */
    public string $linkStyle = 'dashed';
    public function create(): array
    {
        $attribute = [];
        $attribute['type'] = self::TYPE;
        if(!empty($this->linkStyle) && $this->linkStyle === 'solid'){
            $attribute['linkStyle'] = 'solid';
        }
        return $attribute;
    }

    public function appendBody(string|array $content = ''): static
    {
        return $this;
    }
}