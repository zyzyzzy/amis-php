<?php

namespace Aimanong\AmisPhp\Component\Attribute;

trait ButtonToolbar
{
    const TYPE = 'button-toolbar';
    public array $buttons = [];
    public int $packageType = 0; //0 button-toolbar 1 button-group
    public function create(): array
    {
        $data = [];
        $data['type'] = self::TYPE;
        $this->packageType === 0 || $data['type'] = 'button-group';
        $data['buttons'] = $this->buttons;
        return $data;
    }

    public function appendBody(string|array $content = ''): static
    {
        $this->buttons[] = $content;
        return $this;
    }
}