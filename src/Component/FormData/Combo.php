<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;

class Combo extends BaseClass
{
    use FormItem;
    use \Aimanong\AmisPhp\Component\Attribute\Combo;

    /**
     * 添加组合展示的表单项
     * @param array $items
     * @param string $columnClassName 列的类名，可以用它配置列宽度。默认平均分配。
     * @param bool $unique 设置当前列值是否唯一，即不允许重复选择。
     * @return Combo
     */
    public function appendItems(array $items = [], string $columnClassName = '', bool $unique = false): static
    {
        $data = [];
        empty($columnClassName) || $data['columnClassName'] = $columnClassName;
        $unique === false || $data['unique'] = true;
        $this->items[] = [...$items,...$data];
        return $this;
    }

    /**
     * 添加新增分类型组
     * @param string $label 类型名称文本
     * @param string $typeName 类型名称
     * @param array $scaffold 新增的表单项的初始数据
     * @param array $items
     * @param bool $typeSwitchable 是否可以切换类型
     * @return $this
     */
    public function appendConditions(string $label = '', string $typeName = '', array $scaffold = [], array $items = [], bool $typeSwitchable = false): static
    {
        $data = [];
        $data['label'] = $label;
        $data['$scaffold'] = [...$scaffold,['type'=>$typeName]];
        $typeSwitchable === false || $data['typeSwitchable'] = true;
        $data['test'] = "this.type ===\"".$typeName."\"";
        $data['items'] = $items;
        $this->conditions[] = $data;
        return $this;
    }

    /**
     * 打开Tabs模式
     * @param string $tabsStyle
     * @param string $tabsLabelTpl
     * @return $this
     */
    public function openTabsMode(string $tabsStyle = 'line', string $tabsLabelTpl = '${index|plus}'): static
    {
        $this->tabsMode = true;
        $tabsStyle === 'line' || $this->tabsStyle = $tabsStyle;
        $tabsLabelTpl === '${index|plus}' || $this->tabsLabelTpl = $tabsLabelTpl;
        return $this;
    }
}