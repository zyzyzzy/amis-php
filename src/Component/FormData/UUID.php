<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class UUID extends BaseClass
{
    public int $length = 0;
    public function appendBody(array|string $content = ''): static
    {
        return $this;
    }

    public function create(): array
    {
        $data = [];
        $data['type'] = 'uuid';
        $data['name'] = $this->name;
        empty($this->length) || $data['length'] = $this->length;
        return $data;
    }
}