<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class Form extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Form;

    public function appendRules(string $rule = '', string $message = '', array $name = []): static
    {
        $this->rules[] = [
            'rule' => $rule,
            'message' => $message,
            'name' =>$name
        ];
        return $this;
    }
}