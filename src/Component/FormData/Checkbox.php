<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;

class Checkbox extends Switchs
{
    use FormItem;
    use \Aimanong\AmisPhp\Component\Attribute\Switchs;
    public function create(): array
    {
        $data = parent::create();
        $data['type'] = 'checkbox';
        empty($this->option) || $data['option'] = $this->option;
        return $data;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}