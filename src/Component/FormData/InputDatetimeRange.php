<?php

namespace Aimanong\AmisPhp\Component\FormData;

class InputDatetimeRange extends InputDatetime
{
    public function create(): array
    {
        $data =  parent::create();
        $data['type'] = 'input-datetime-range';
        $data['timeFormat'] = 'HH:mm:ss';
        if(array_key_exists('shortcuts',$data)){
            $data['ranges'] = $data['shortcuts'];
            unset($data['shortcuts']);
        }
        return $data;
    }
}