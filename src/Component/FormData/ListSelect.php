<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\Options;
use Aimanong\AmisPhp\Component\BaseClass;

class ListSelect extends BaseClass
{
    use Options;
    use \Aimanong\AmisPhp\Component\Attribute\ListSelect;
}