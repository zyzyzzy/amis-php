<?php

namespace Aimanong\AmisPhp\Component\FormData;

class InputDatetime extends InputDate
{
    public array $timeConstraints = [];
    public string $format = 'YYYY-MM-DD HH:mm:ss'; //时间值格式
    public string $inputFormat = 'YYYY-MM-DD HH:mm:ss';  //时间显示格式
    public function create(): array
    {
        $data = parent::create();
        $data['type'] = 'input-datetime';
        empty($this->format) || $data['format'] = $this->format;
        empty($this->inputFormat) || $data['inputFormat'] = $this->inputFormat;
        empty($this->timeConstraints) || $data['timeConstraints'] = $this->timeConstraints;
        return $data;
    }

    /**
     * 限制时分秒
     * @param array $hours
     * @param array $minutes
     * @param array $seconds
     * @return $this
     */
    public function setTimeConstraints(array $hours = [0,23], array $minutes = [0,59], array $seconds = [0,59]): static
    {
        $this->timeConstraints['hours'] = [
            'min' => $hours[0],
            'max' => $hours[1]
        ];
        $this->timeConstraints['minutes'] = [
            'min' => $minutes[0],
            'max' => $minutes[1]
        ];
        $this->timeConstraints['seconds'] = [
            'min' => $seconds[0],
            'max' => $seconds[1]
        ];
        return $this;
    }
}