<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;
use Aimanong\AmisPhp\Component\enum\DateShortcuts;

class InputDate extends BaseClass
{
    use FormItem;
    use \Aimanong\AmisPhp\Component\Attribute\InputDate;

    /**
     * 添加日期的快捷
     * @param DateShortcuts $shortcuts
     * @param int|string $number
     * @return $this
     */
    public function appendShortcuts(DateShortcuts $shortcuts, int|string $number = ''): static
    {
        $this->shortcuts[] = $number.$shortcuts->value;
        return $this;
    }
}