<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\Attribute\Options;
use Aimanong\AmisPhp\Component\BaseClass;

class Select extends BaseClass
{
    use FormItem;
    use Options;
    use \Aimanong\AmisPhp\Component\Attribute\Select;

    /**
     * @param string $label
     * @param string $value
     * @param array $children
     * @return $this
     */
    public function appendOptions(string $label = '', string $value = '', array $children = []): static
    {
        if(empty($children)){
            $this->options[] = [
                'label' => $label,
                'value' => $value
            ];
        }else{
            $this->options[] = [
                'label' => $label,
                'children' => $children
            ];
        }

        return $this;
    }

    /**
     * @param array $columns
     * @param array $options
     * @return $this
     */
    public function setTableMode(array $columns = [], array $options = []): static
    {
        $this->selectMode = 'table';
        $this->columns = $columns;
        $this->options = $options;
        return $this;
    }

    /**
     * @param string $title
     * @param array $addControls
     * @param string $addApi
     * @return $this
     */
    public function addOption(string $title = '选项', array $addControls = [], string $addApi = ''): static
    {
        $this->creatable = true;
        $this->optionLabel = $title;
        $this->addControls = [];
        $this->addApi = $addApi;
        return $this;
    }

    /**
     * @param string $title
     * @param array $editControls
     * @param string $editApi
     * @return $this
     */
    public function editOption(string $title = '选项', array $editControls = [], string $editApi = ''): static
    {
        $this->editable = true;
        $this->optionLabel = $title;
        $this->editControls = $editControls;
        $this->editApi = $editApi;
        return $this;
    }

    /**
     * @param string $deleteApi
     * @return $this
     */
    public function deleteOption(string $deleteApi = ''): static
    {
        $this->removable = true;
        $this->deleteApi = $deleteApi;
        return $this;
    }
}