<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class ButtonToolbar extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\ButtonToolbar;
}