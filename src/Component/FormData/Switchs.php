<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;

class Switchs extends BaseClass
{
    use FormItem;
    use \Aimanong\AmisPhp\Component\Attribute\Switchs;

    /**
     * 设置开启状态时的文本内容或Icon图标
     * @param string $infoText
     * @param string $icon
     * @return $this
     */
    public function setOnText(string $infoText = '', string $icon = ''): static
    {
        if(!empty($infoText)){
            $this->onText = $infoText;
        }elseif(!empty($icon)){
            $this->onText = [
                'type' => 'icon',
                'icon' => $icon
            ];
        }
        return $this;
    }

    /**
     * 设置关闭状态时的文本内容或Icon图标
     * @param string $infoText
     * @param string $icon
     * @return $this
     */
    public function setOffText(string $infoText = '', string $icon = ''): static
    {
        if(!empty($infoText)){
            $this->offText = $infoText;
        }elseif(!empty($icon)){
            $this->offText = [
                'type' => 'icon',
                'icon' => $icon
            ];
        }
        return $this;
    }
}