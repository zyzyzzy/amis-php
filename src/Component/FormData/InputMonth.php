<?php

namespace Aimanong\AmisPhp\Component\FormData;

class InputMonth extends InputDate
{
    public string $format = 'YYYY-MM'; //时间值格式
    public string $inputFormat = 'YYYY年MM月';  //时间显示格式
    public function create(): array
    {
        $data = parent::create();
        $data['type'] = 'input-month';
        empty($this->format) || $data['format'] = $this->format;
        empty($this->inputFormat) || $data['inputFormat'] = $this->inputFormat;
        return $data;
    }
}