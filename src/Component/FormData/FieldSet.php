<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class FieldSet extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\FieldSet;
}