<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class Group extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Group;
}