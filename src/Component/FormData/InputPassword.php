<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;

class InputPassword extends BaseClass
{
    use FormItem;
    public bool $revealPassword = true;
    public string $prefix = '';  //前缀
    public string $suffix = ''; //后缀
    public array $addOn = [];
    public function appendBody(array|string $content = ''): static
    {
        return $this;
    }

    public function create(): array
    {
        $data = [];
        $data['type'] = 'input-password';
        $data['name'] = $this->name;
        (empty($this->label) && $this->label !== false) || $data['label'] = $this->label;
        empty($this->size) || $data['size'] = $this->size;
        empty($this->placeholder) || $data['placeholder'] = $this->placeholder;
        empty($this->className) || $data['className'] = $this->className;
        empty($this->prefix) || $data['prefix'] = $this->prefix;
        empty($this->suffix) || $data['suffix'] = $this->suffix;
        empty($this->addOn) || $data['addOn'] = $this->addOn;
        $this->revealPassword === true || $data['revealPassword'] = false;
        return $data;
    }
}