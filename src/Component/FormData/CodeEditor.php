<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;
class CodeEditor extends BaseClass
{
    use FormItem;
    use \Aimanong\AmisPhp\Component\Attribute\CodeEditor;
}