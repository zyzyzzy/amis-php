<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class ChainedSelect extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\ChainedSelect;
    /**
     * level: 当前拉取数据时的层级,
     * parentId: 上一级选项的值，数据格式基于配置
     */
}