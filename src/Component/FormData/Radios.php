<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\Options;
use Aimanong\AmisPhp\Component\BaseClass;

class Radios extends BaseClass
{
    use Options;
    use \Aimanong\AmisPhp\Component\Attribute\Radios;
    public function __construct(...$args)
    {
        $this->inline = true;
        parent::__construct($args);
    }
}