<?php

namespace Aimanong\AmisPhp\Component\FormData;

class InputDateRange extends InputDate
{
    public string $minDuration = '';  //限制最小跨度，如： 2days
    public string $maxDuration = '';  //限制最大跨度，如：1year
    public function create(): array
    {
        $data = parent::create();
        $data['type'] = 'input-date-range';
        empty($this->minDuration) || $data['minDuration'] = $this->minDuration;
        empty($this->maxDuration) || $data['maxDuration'] = $this->maxDuration;
        if(array_key_exists('shortcuts',$data)){
            $data['ranges'] = $data['shortcuts'];
            unset($data['shortcuts']);
        }
        return $data;
    }
}