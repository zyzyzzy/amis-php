<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\Options;
use Aimanong\AmisPhp\Component\BaseClass;

class Checkboxes extends BaseClass
{
    use Options;
    use \Aimanong\AmisPhp\Component\Attribute\Checkboxes;

    public function __construct(...$args)
    {
        parent::__construct(...$args);
         $this->inline = true;//是否显示一行
    }

    /**
     * @param string $label
     * @param string $value
     * @param array $children
     * @return $this
     */
    public function appendOptions(string $label = '', string $value = '', array $children = []): static
    {
        if(empty($children)){
            $this->options[] = [
                'label' => $label,
                'value' => $value
            ];
        }else{
            $this->options[] = [
                'label' => $label,
                'children' => $children
            ];
            $this->inline = false;
        }

        return $this;
    }

    /**
     * @param string $createBtnLabel
     * @param array $addControls
     * @param string|array $addApi
     * @return $this
     */
    public function setAddButton(string $createBtnLabel = '新增选项',array $addControls = [], string|array $addApi = []): static
    {
        $this->creatable = true;
        empty($createBtnLabel) || $createBtnLabel === '新增选项' || $this->createBtnLabel =$createBtnLabel;
        empty($addControls) || $this->addControls = $addControls;
        empty($addApi) || $this->addApi = $addApi;
        return $this;
    }

    /**
     * @param array $editControls
     * @param string|array $editApi
     * @return $this
     */
    public function setEditButton(array $editControls = [], string|array $editApi = ''): static
    {
        $this->editable = true;
        empty($editControls) || $this->editControls = $editControls;
        empty($editApi) || $this->editApi = $editApi;
        return $this;
    }

    /**
     * @param string|array $deleteApi
     * @return $this
     */
    public function setDeleteButton(string|array $deleteApi = ''): static
    {
        $this->removable = true;
        empty($deleteApi) || $this->deleteApi = $deleteApi;
        return $this;
    }
}