<?php

namespace Aimanong\AmisPhp\Component\FormData;

class InputMonthRange extends InputMonth
{
    public function create(): array
    {
        $data = parent::create(); //
        $data['type'] = 'input-month-range';
        return $data;
    }
}