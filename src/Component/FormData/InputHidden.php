<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class InputHidden extends BaseClass
{
    public string|int|array $value = '';
    public function appendBody(array|string $content = ''): static
    {
        return $this;
    }

    public function create(): array
    {
        $data = [];
        $data['type'] = 'hidden';
        $data['name'] = $this->name;
        empty($this->value) || $data['value'] = $this->value;
        return $data;
    }
}