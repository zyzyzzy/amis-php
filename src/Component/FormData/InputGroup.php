<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\BaseClass;

class InputGroup extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\InputGroup;
}