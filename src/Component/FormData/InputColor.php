<?php

namespace Aimanong\AmisPhp\Component\FormData;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;

class InputColor extends BaseClass
{
    use FormItem;
    use \Aimanong\AmisPhp\Component\Attribute\InputColor;
}