<?php

namespace Aimanong\AmisPhp\Component\Feedback;

use Aimanong\AmisPhp\Component\BaseClass;

class Dialog extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Dialog;
}