<?php

namespace Aimanong\AmisPhp\Component\Feedback;

use Aimanong\AmisPhp\Component\BaseClass;

class Drawer extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Drawer;
}