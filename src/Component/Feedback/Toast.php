<?php

namespace Aimanong\AmisPhp\Component\Feedback;

use Aimanong\AmisPhp\Component\BaseClass;

class Toast extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Toast;
}