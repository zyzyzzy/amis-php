<?php

namespace Aimanong\AmisPhp\Component\DataDisplay;

use Aimanong\AmisPhp\Component\Attribute\FormItem;
use Aimanong\AmisPhp\Component\BaseClass;
use Aimanong\AmisPhp\Component\FormData\Switchs;

class CRUD extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\CRUD;

    /**
     * 添加列数据
     * @param string $label
     * @param string $name
     * @param bool $sortable 排序检索
     * @param array $filterable 快速过滤 相当于 select
     * @param array|bool $quickEdit //快速编辑
     * @param array $searchable //生成查询的列组件
     * @return $this
     */
    public function appendColumns(string $label = '', string $name = '', bool $sortable = false, array $filterable = [] , array|bool $quickEdit = false , array $searchable = []): static
    {
        $data = [
            'label' => $label,
            'name' => $name,
            'canAccessSuperData' => false, //禁止访问父级数据域
        ];
        $sortable === false || $data['sortable'] = true;
        empty($filterable) || $data['filterable'] = $filterable;
        empty($quickEdit) || $data['quickEdit'] = $quickEdit;
        empty($searchable) || $data['searchable'] = $searchable;
        $this->columns[] = $data;
        return $this;
    }

    /**
     * 操作栏
     * @param string $label
     * @param array $buttons
     * @return $this
     */
    public function appendOperation(string $label = '', array $buttons = []): static
    {
        $data = [
            'type' => 'operation',
            'label' => $label,
            'buttons' => $buttons
        ];
        $this->columns[] = $data;
        return $this;
    }

    /**
     * 添加顶部组件
     * @param array $component
     * @return $this
     */
    public function appendHeaderToolbar(array $component = []): static
    {
        $this->headerToolbar[] = $component;
        return $this;
    }

    /**
     * @param bool|array $body
     * @param bool $mode 是否内联
     * @param string $quickEditEnabledOn 启动快速编辑的条件
     * @param string|array|bool $saveImmediately 快速编辑即时保存接口
     * @return array
     */
    public function setQuickEdit(bool|array $body = true, bool $mode = false, string $quickEditEnabledOn = '', string|array|bool $saveImmediately = true): array
    {
        $data = $body;
        if(!is_bool($body)) {
            $mode === false || $data['mode'] = 'inline';
            empty($quickEditEnabledOn) || $data['quickEditEnabledOn'] = $quickEditEnabledOn;
            if (!empty($saveImmediately)) {
                if (is_bool($saveImmediately)) {
                    $data['saveImmediately'] = true;
                } elseif (is_string($saveImmediately) || is_array($saveImmediately)) {
                    $data['saveImmediately']['api'] = $saveImmediately;
                }
            }
        }
        return $data;
    }

    /**
     * 开启自动生成查询区域
     * @param int $columnsNum //过滤条件单行列数
     * @param bool $showBtnToolbar //是否显示设置查询字段
     * @return $this
     */
    public function setAutoGenerateFilter(int $columnsNum = 3, bool $showBtnToolbar = true): static
    {
        $this->autoGenerateFilter = [
            'columnsNum' => $columnsNum,
            'showBtnToolbar' => $showBtnToolbar
        ];
        return $this;
    }

    /**
     * 设置默认请求参数
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function setDefaultParams(string $name = '', string $value = ''): static
    {
        $this->defaultParams[$name] = $value;
        return $this;
    }

    /**
     * 查询条件表单
     * @param string $title
     * @param array $body
     * @param bool $debug
     * @return $this
     */
    public function setFilter(string $title = '', array $body = [], bool $debug = false): static
    {
        $this->filter = [
            'title' => $title,
            'body' => $body
        ];
        $debug === false || $this->filter['debug'] = true;
    }

    /**
     * 设置批量删除按钮
     * @param string $label
     * @param string $confirmText
     * @param string|array $api ${ids|raw}
     * @return array
     */
    public function setBatchDeleteButton(string $label = '批量删除', string $confirmText = '确认要批量删除吗?', string|array $api = ''): array
    {
        return [
            'label' => $label,
            'actionType' => 'ajax',
            'api' => $api,
            'confirmText' => $confirmText
        ];
    }

    /**
     * 批量修改
     * @param string $label
     * @param string $title
     * @param string|array $api
     * @param array $body
     * @return array
     */
    public function setBatchEditButton(string $label = '批量修改', string $title = '批量修改', string|array $api = '', array $body = []): array
    {
        return [
            'label' => $label,
            'actionType' => 'dialog',
            'dialog' => [
                'title' => $title,
                'body' => [
                    'type' => 'form',
                    'api' => $api,
                    'body' => [
                        [
                            'type' => 'hidden',
                            'name' => 'ids'
                        ],
                        ...$body
                    ]
                ]
            ]
        ];
    }
}