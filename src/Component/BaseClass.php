<?php

namespace Aimanong\AmisPhp\Component;


abstract class BaseClass
{
    public string $name = '';
    public string $id = '';
    public array $onEvent = [];  //事件
    public function __construct(...$args)
    {
        mt_srand(time());
//        $this->name = 'name:'.time().mt_rand(100000000,999999999);
        $this->name = 'name:'.$this->msectime();
//        $this->id = 'id:'.time().mt_rand(100000000,999999999);
        $this->id = 'id:'.$this->msectime();
        if(!empty($args)){
            foreach ($args as $key => $value){
//                echo('['.$key.'] => '.$value.'<br>');
                $this->$key = $value;
            }
        }
    }
    private function msectime(): string
    {
        return str_replace('.','',str_replace(' ','',microtime()));
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * 添加事件行为方法
     * @param $name
     * click 点击时触发
     * mouseenter 鼠标移入时触发
     * mouseleave 鼠标移出时触发
     * @param $arguments
     * @return $this
     */
    public function __call($name, $arguments)
    {
        if($name === 'click' || $name === 'mouseenter' || $name === 'mouseleave' || $name === 'change'){
            $actionName = $arguments[0];
            $args = $arguments[1];
            $this->onEvent[$name]['actions'][] = EventAction::$actionName(...$args);
        }
        return $this;
    }
    /**
     * 添加主体数据
     * @param string|array $content
     * @return $this
     */
    abstract public function appendBody(string|array $content = ''): static;
    abstract public function create(): array;
}