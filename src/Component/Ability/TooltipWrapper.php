<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class TooltipWrapper extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\TooltipWrapper;
}