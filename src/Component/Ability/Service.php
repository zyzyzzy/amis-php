<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class Service extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Service;
}