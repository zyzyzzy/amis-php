<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class AnchorNav extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\AnchorNav;
    public function appendLinks(string $title = '', string $href = '', array $body = [], string $className = ''): static
    {
        $data = [];
        $data['title'] = $title;
        empty($href) || $data['href'] = $href;
        $data['body'] = $body;
        empty($className) || $data['className'] = $className;
        return $this->appendBody($data);
    }
}