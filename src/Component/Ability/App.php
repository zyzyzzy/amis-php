<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class App extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\App;

    /**
     * 添加菜单栏
     * @param string $label 菜单名
     * @param string $url
     * @param string $icon 菜单图标
     * @param string|array $schema 页面的配置
     * @param string|array $schemaApi 页面的配置的拉取接口, $schema 和 $schemaApi 只能选一个
     * @param string $link 如果想配置外部链接菜单,配置这个
     * @param string $redirect 跳转到目标页面
     * @param string $rewrite 地址不变,改为渲染其它路径的页面
     * @param bool $visible 是否显示菜单
     * @param array $children //子菜单
     * @param string $className
     * @return array
     */
    public function appendPages(string $label = '', string $url = '', string $icon = '', string|array $schema = [], string|array $schemaApi = '', string $link = '', string $redirect = '', string $rewrite = '', bool $visible = true, array $children = [], string $className = ''): array
    {
        $data = [];
        $data['label'] = $label;
        $data['url'] = $url ? : md5($label.str_replace('.','',str_replace(' ','',microtime())));
        empty($icon) || $data['icon'] = $icon;
        if(!empty($schema)){
            $data['schema'] = $schema;
        }elseif(!empty($schemaApi)){
            $data['schemaApi'] = $schemaApi;
        }
        empty($link) || $data['link'] = $link;
        empty($redirect) || $data['redirect'] = $redirect;
        empty($rewrite) || $data['rewrite'] = $rewrite;
        $visible === true || $data['visible'] = false;
        empty($className) || $data['className'] = $className;
        empty($children) || $data['children'] = $children;
        return $data;
    }
}