<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;
use Aimanong\AmisPhp\Component\enum\ActionType;
use Aimanong\AmisPhp\Component\enum\Level;
use Aimanong\AmisPhp\Component\Feedback\Dialog;
use Aimanong\AmisPhp\Component\Feedback\Drawer;

class Action extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Action;

    /**
     * @param string|array $api ajax提交地址
     * @param string $label 按钮文本
     * @param string $confirmText 确认提示内容
     * @param string $redirect 请求成功后跳转地址
     * @param string|array $reload 请求成功后刷新目标,目标的 name 值,如果多个用英文的逗号隔开
     * @param string $feedback_title 请求成功后,反馈弹框标题
     * @param string $feedback_body 请求成功后,反馈弹框内容
     * @param string $messages_success 请求成功后toast提示
     * @param string $messages_failed 请求失败后toast提示
     * @return array
     */
    public function 异步请求按钮(
        string|array $api = '',
        string $label = 'ajax请求',
        string $confirmText = '',
        string $redirect = '',
        string|array $reload = '',
        string $feedback_title = '',
        string $feedback_body = '',
        string $messages_success = '',
        string $messages_failed = ''
    ): array
    {
        $data = $this->create();
        $ajaxData  = [];
        $ajaxData['actionType'] = ActionType::异步请求->value;
        $ajaxData['api'] = $api;
        empty($label) || $ajaxData['label'] = $label;
        empty($confirmText) || $ajaxData['confirmText'] = $confirmText;
        empty($redirect) || $ajaxData['redirect'] = $redirect;
        if(!empty($reload)){
            if(is_string($reload)){
                $ajaxData['reload'] = $reload;
            }elseif(is_array($reload)){
                $ajaxData['reload'] = implode(',',$reload);
            }
        }
        if(!empty($feedback_title) && !empty($feedback_body)){
            $ajaxData['feedback'] = [
                'title' => $feedback_title,
                'body' => $feedback_body
            ];
        }
        empty($messages_success) || $ajaxData['messages']['success'] = $messages_success;
        empty($messages_failed) || $ajaxData['messages']['failed'] = $messages_failed;
        return [...$data,...$ajaxData];
    }

    /**
     * 主要用于发验证码的场景，通过设置倒计时 countDown（单位是秒），让点击按钮后禁用一段时间
     * @param string $label 按钮文本
     * @param int $countDown 倒计时多少秒
     * @param string|array $api 提交地址
     * @param string $countDownTpl 禁用时显示的文本,${timeLeft} 变量是剩余的时间(秒)
     * @return array
     */
    public function 倒计时按钮(string $label = '发送验证码',int $countDown = 60, string|array $api = '', string $countDownTpl = ""): array
    {
        $data = $this->create();
        $countDownData = [];
        $countDownData['name'] = $this->name;
        empty($label) || $countDownData['label'] = $label;
        $countDownData['countDown'] = $countDown;
        $countDownData['countDownTpl'] = $countDownTpl ? :'${timeLeft} 秒后重发';
        $countDownData['actionType'] = ActionType::异步请求->value;
        $countDownData['api'] = $api;
        return [...$data,...$countDownData];
    }

    /**
     * 页面跳转
     * @param string $jumpType link 单页跳转(amis平台内的页面) url 直接跳转(http/https的网页跳转)
     * @param string $jumpAddress 跳转的的地址
     * @param bool $blank 是否在新的tab页面打开 默认否
     * @param string $label 按钮的文本
     * @return array
     */
    public function 页面跳转按钮(string $jumpType = 'url' ,string $jumpAddress = '', bool $blank = false, string $label = '打开'): array
    {
        $data = $this->create();
        $jumpData = [];
        match ($jumpType){
          'link' => $jumpData['actionType'] = 'link',
          default => $jumpData['actionType'] = 'url'
        };
        $jumpData[$jumpData['actionType']] = $jumpAddress;
        $jumpData['actionType'] === 'url' && $blank === true && $jumpData['blank'] = true;
        empty($label) || $jumpData['label'] = $label;
        return [...$data,...$jumpData];
    }

    /**
     * 弹框
     * @param Dialog $dialog
     * @return array
     */
    public function 弹框按钮(Dialog $dialog): array
    {
        return [...$this->create(),...$dialog->create()];
    }

    public function 抽屉按钮(Drawer $drawer): array
    {
        return [...$this->create(),...$drawer->create()];
    }

    /**
     * 复制按钮
     * @param string $label 按钮文本
     * @param string $content 复制内容
     * @return array
     */
    public function 复制按钮(string $label = '', string $content = ''): array
    {
        $copyData = [];
        $copyData['actionType'] = 'copy';
        empty($label) || $copyData['label'] = $label;
        $copyData['content'] = $content;
        return [...$this->create(),...$copyData];
    }

    /**
     * 刷新按钮
     * @param string $label
     * @param string|array $target
     * @return array
     */
    public function 刷新按钮(string $label = '', string|array $target = ''): array
    {
        $reloadData = [];
        $reloadData['actionType'] = 'reload';
        empty($label) || $reloadData['label'] = $label;
        if(!empty($target)){
            if(is_string($target)){
                $ajaxData['target'] = $target;
            }elseif(is_array($target)){
                $ajaxData['target'] = implode(',',$target);
            }
        }
        return [...$this->create(),...$reloadData];
    }

    public function 刷新指定ID按钮(string $componentId = ''): array
    {
        $data = $this->create();
        $data['onEvent']['click']['actions'][] = [
            'actionType' => 'reload',
            'componentId' => $componentId
        ];
        $data['icon'] = 'fas fa-sync-alt';
        return $data;
    }

    public function 重置表单按钮(string $label = '重置'): array
    {
        return [...$this->create(),...['actionType'=>'reset','label'=>$label]];
    }

    public function 清空表单按钮(string $label = '清空'): array
    {
        return [...$this->create(),...['actionType'=>'clear','label'=>$label]];
    }

    public function 提交表单按钮(string $label = '提交', string $level = 'primary'): array
    {
        return [...$this->create(),...['actionType'=>'submit','label'=>$label,'level'=>$level]];
    }

    public function 取消关闭按钮(string $label = '取消'): array
    {
        $data = $this->create();
        $data['actionType'] = 'close';
        !empty($data['label']) || $data['label'] = $label;
        return $data;
    }

    public function 全屏按钮(string $icon = 'fas fa-expand-arrows-alt'): array
    {
        $data = $this->create();
        $data['onEvent']['click']['actions'][] = [
            'actionType' => 'custom',
            'script' => "if(document.fullscreenElement||document.mozFullScreenElement||document.webkitFullscreenElement||document.msFullscreenElement){if(document.exitFullscreen){document.exitFullscreen()}else if(document.mozCancelFullScreen){document.mozCancelFullScreen()}else if(document.webkitExitFullscreen){document.webkitExitFullscreen()}else if(document.msExitFullscreen){document.msExitFullscreen()}}else{const element=document.documentElement;if(element.requestFullscreen){element.requestFullscreen()}else if(element.mozRequestFullScreen){element.mozRequestFullScreen()}else if(element.webkitRequestFullscreen){element.webkitRequestFullscreen()}else if(element.msRequestFullscreen){element.msRequestFullscreen()}}"
        ];
        $data['icon'] = $icon;
        return $data;
    }
}