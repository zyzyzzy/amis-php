<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class ButtonGroup extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\ButtonGroup;
}