<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class DropDownButton extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\DropDownButton;

    /**
     * @param string $label
     * @param string $icon
     * @param array $buttons
     * @return $this
     */
    public function appendButtonsChildren(string $label = '', string $icon = '', array $buttons = []): static
    {
        $data = [
            'label' => $label,
            'icon' => $icon,
            'children' => $buttons
        ];
        return $this->appendBody($data);
    }
}