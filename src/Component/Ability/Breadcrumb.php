<?php

namespace Aimanong\AmisPhp\Component\Ability;

use Aimanong\AmisPhp\Component\BaseClass;

class Breadcrumb extends BaseClass
{
    use \Aimanong\AmisPhp\Component\Attribute\Breadcrumb;

    /**
     * 添加子项
     * @param string $label
     * @param string $href
     * @param string $icon
     * @return $this
     */
    public function createItems(string $label = '', string $href = '', string $icon = ''): static
    {
        $data = [
            'label' => $label,
            'href' => $href,
            'icon' => $icon
        ];
        return $this->appendBody($data);
    }
}